(function(){
    "use strict";

    var port = "8080";
    var serverUri = "http://localhost:" + port  + "/grupos";

    casper.test.begin('user gets collections', 1, function(test) {
        casper.start(serverUri, function() {
            
        }).then(function(){
            console.log(this.getPageContent());
            test.assertVisible('div[id="grupos-panel"]', "Groups panel is visible");
        }).run(function() {
            test.done();
        });
    });
}());