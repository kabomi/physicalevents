/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

"use strict";

(function(){

    var MongoClient = require('mongodb').MongoClient;
    var format = require('util').format;
    var util = require("../../lib/server_lib");
    var HOST = process.env.MONGODB_HOST ? process.env.MONGODB_HOST : 'localhost';
    var PORT = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : 27017;
    var dbName = "berlin-test";
    var URI = format("mongodb://%s:%s/%s?w=1", HOST, PORT, dbName);
    var collectionName = 'collectionName';
    var testData;

    describe("mongodb", function(){
        beforeEach(function(){
            testData = [
                {'_id': 0, 'name': 'test0'},
                {'_id': 1, 'name': 'test1'}];
        });
        it("listens to a simple request", function(done){
            console.log("mongodb uri:" + URI);
            MongoClient.connect(URI, function(err, db) {
                expect(err).toBeNull();
                db.close();
                done();
            });
        });
        it("creates a collection and inserts data", function(done){
            function onInsertingTestData(db, collection){
                collection.count(function(err, count) {
                    expect(count).toBeGreaterThan(0);
                    db.close();
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("retrieves data from a collection", function(done){
            function onInsertingTestData(db, collection){
                collection.find({}).each(function(err, item){
                    if(item !== null){
                        expect(item.name).toContain('test');
                    }else{
                        collection.drop(function(err, collection) {
                          db.close();
                          done();
                        });
                    }
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
    });
    describe("mongodb_repo", function(){
        var server;
        var repo = require('../mongodb_repo').init(dbName);
        var item;

        beforeEach(function(){
            server = require("../server").init({
                repo: repo
            });
            testData = [
                {'_id': 0, 'name': 'test0'},
                {'_id': 1, 'name': 'test1'}];
            item = testData[0];
        });

        it("gets a collection", function(done){
            function onInsertingTestData(db){
                repo.getCollection(collectionName, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj[0].id).toBe(0);
                    expect(responseObj[1].name).toBe('test1');
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("gets a collection filtered", function(done){
            function onInsertingTestData(db){
                var filter = {_id: 0};
                repo.getCollectionByFilter(collectionName, filter, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj[0].id).toBe(0);
                    expect(responseObj[0].name).toBe('test0');
                    expect(responseObj.length).toBe(1);
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("gets an item from a collection providing item id or name", function(done){
            var itemValue = 0;
            function onInsertingTestData(db){
                repo.getCollectionItem(collectionName, itemValue, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.id).toBe(0);
                    expect(responseObj.name).toBe('test0');
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
            itemValue = 'test0';
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("gets an item from a collection providing item generated _id", function(done){
            var itemValue = 0;
            testData = [
                {'name': 'test0'},
                {'name': 'test1'}];
            function onInsertingTestData(db, collection, docs){
                repo.getCollectionItem(collectionName, docs[0]._id, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.name).toBe('test0');
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("gets an item from a collection providing item generated hex string _id", function(done){
            var itemValue = 0;
            testData = [
                {'name': 'test0'},
                {'name': 'test1'}];
            function onInsertingTestData(db, collection, docs){
                repo.getCollectionItem(collectionName, docs[0]._id.toHexString(), function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.name).toBe('test0');
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("drops an error when trying to get undefined item from a collection", function(done){
            function onInsertingTestData(db){
                repo.getCollectionItem(collectionName, undefined, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.error).toBeTruthy();
                    done();
                });
            }
            insertTestDataInto(collectionName, onInsertingTestData);
        });
        it("inserts an item to a collection and defines an id field", function(done){
            dropCollection(collectionName, function(db){
                setCollectionItem(collectionName, item, function(opResult){
                    expect(opResult.inserted).toBeTruthy();
                    expect(item.id).toBeDefined();
                    dropCollection(collectionName, function(){done();});
                });
            });
        });
        it("checks that item possible ids(id or name or _id) are defined before perform update operation", function(done){
            item = {id: undefined};
            var obj = {
                callback: function(opResult){
                    expect(opResult.error).toBeTruthy();
                }
            };
            spyOn(obj, 'callback').andCallThrough();
            setCollectionItem(collectionName, item, obj.callback);
            item = {_id: undefined};
            obj = {
                callback: function(opResult){
                    expect(opResult.error).toBeTruthy();
                }
            };
            spyOn(obj, 'callback').andCallThrough();
            setCollectionItem(collectionName, item, obj.callback);
            item = {name: undefined};
            obj = {
                callback: function(opResult){
                    expect(opResult.error).toBeTruthy();
                    done();
                }
            };
            spyOn(obj, 'callback').andCallThrough();
            setCollectionItem(collectionName, item, obj.callback);
            item = undefined;
            obj = {
                callback: function(opResult){
                    expect(opResult.error).toBeTruthy();
                }
            };
            spyOn(obj, 'callback').andCallThrough();
            setCollectionItem(collectionName, item, obj.callback);
        });
        it("updates an item of a collection", function(done){
            checkUpdateCollectionItem(item, item, '_id', done);
        });
        it("updates an item of a collection from its id", function(done){
            var itemToInsert = {_id: 0};
            item = {id: 0, 'name': 'test1'};
            checkUpdateCollectionItem(itemToInsert, item, 'id', done);
        });
        it("updates an item of a collection only from its name", function(done){
            item = {'name': 'test1'};
            checkUpdateCollectionItem(item, item, 'name', done);
        });
        it("updates an item of a collection only from its generated _id", function(done){
            item = {'test': 'test1'};
            checkUpdateCollectionItem(item, item, '_id', done);
        });
        it("updates an item of a collection only from its generated hex string _id", function(done){
            item = {'test': 'test1'};
            checkUpdateCollectionItem(item, item, 'hex_id', done);
        });
        it("updates an item of a collection of more than 1 item only from its generated _id", function(done){
            item = {'test': 'test1'};
            var items = [item, {'item2': 'item2test'}];
            checkUpdateCollectionItem(items, item, '_id', done);
        });
        function checkUpdateCollectionItem(itemsToInsert, itemToTest, updateIdFieldToUpdate, done){
            setCollectionItem(collectionName, itemsToInsert, function(opResult){
                var updatedItem = {
                    test: 'test2'};
                if(updateIdFieldToUpdate === 'hex_id'){
                    updateIdFieldToUpdate = '_id';
                    updatedItem[updateIdFieldToUpdate] = itemToTest[updateIdFieldToUpdate].toHexString();
                }else{
                    updatedItem[updateIdFieldToUpdate] = itemToTest[updateIdFieldToUpdate];
                }
                var itemToTest_id = ((typeof(itemToTest._id) === 'object')?
                    itemToTest._id.toHexString():
                    itemToTest._id);
                if(updateIdFieldToUpdate === 'id'){
                    itemToTest_id = itemsToInsert._id;
                }
                expect(opResult.inserted).toBeTruthy();
                setCollectionItem(collectionName, updatedItem, function(opResult){
                    expect(opResult.updated).toBeTruthy();
                    repo.getCollectionItem(collectionName, updatedItem[updateIdFieldToUpdate], function(responseData){
                        var responseObj = JSON.parse(responseData);
                        expect(responseObj.error).toBeUndefined();
                        expect(responseObj._id).toBe(itemToTest_id);
                        expect(responseObj.test).toBe('test2');
                        dropCollection(collectionName, function(db){
                            db.close();
                            done();
                        });
                    });
                });
            });
        }
        it("removes an item from a collection given its _id", function(done){
            var itemValue = item._id;
            checkRemovesCollectionItemGivenItemValue(itemValue, done);
        });
        it("removes an item from a collection given its name", function(done){
            var itemValue = item.name;
            checkRemovesCollectionItemGivenItemValue(itemValue, done);
        });
        it("removes an item from a collection given its id", function(done){
            item = {'id': 0, 'name': 'test0'};
            var itemValue = item.id;
            checkRemovesCollectionItemGivenItemValue(itemValue, done);
        });
        it("removes an item from a collection given its generated _id", function(done){
            item = {'name': 'test0'};
            setCollectionItem(collectionName, item, function(){
                repo.removeCollectionItem(collectionName, item._id, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.deleted).toBeTruthy();
                    done();
                });
            });
        });
        it("removes an item from a collection given its generated hex string _id", function(done){
            item = {'name': 'test0'};
            setCollectionItem(collectionName, item, function(){
                repo.removeCollectionItem(collectionName, item._id.toHexString(), function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.deleted).toBeTruthy();
                    done();
                });
            });
        });
        function checkRemovesCollectionItemGivenItemValue(itemValue, done){
            setCollectionItem(collectionName, item, function(){
                repo.removeCollectionItem(collectionName, item.name, function(responseData){
                    var responseObj = JSON.parse(responseData);
                    expect(responseObj.deleted).toBeTruthy();
                    done();
                });
            });
        }
        function setCollectionItem(collectionName, item, callback){
            repo.setCollectionItem(collectionName, item, function(response, opResult){
                if(response){
                    callback(opResult);
                }
            });
        }
    });
    function insertTestDataInto(collectionName, callback){
        dropCollection(collectionName, function(db, collection) {
            collection.insert(testData, function(err, docs){
                callback(db, collection, docs);
            });
        });
    }
    function dropCollection(collectionName, callback){
        MongoClient.connect(URI, function(err, db){
            var collection = db.collection(collectionName);
            collection.drop(function(err, collectionDeleted) {
                callback(db, collection);
            });
        });
    }
})();