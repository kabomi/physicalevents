/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

"use strict";

(function(){

    var util = require("../../lib/server_lib");
    var repo = require("../mongodb_repo").init("berlin-test");

    describe("Calculations", function(){
        var calc, item, fichas, lesiones;

        beforeEach(function(){
            calc = require("../calculations").init({repo: repo});
            item = {id: 0, name: 'name0', grupo: 0, lesion: 30, caracteristicas: [
                        {id: 0, name:'name0', value:10},
                        {id: 1, name:'name1', value:20},
                        {id: 2, name:'name2', value:30},
                        {id: 3, name:'name3', value:40}],
                    lesiones: [{id: 0, name:'name0', description: 'testDescription'}]
            };
            fichas = [
                    {id: 0, name:'name0', nick:'nick0', caracteristicas: [
                        {id: 0, name:'name0', value: 1},
                        {id: 1, name:'name1', value: 1},
                        {id: 2, name:'name2', value: 1},
                        {id: 3, name:'name3', value: 1}
                    ]},
                    {id: 1, name:'name1', nick:'nick1', caracteristicas: [
                        {id: 0, name:'name0', value: 2},
                        {id: 1, name:'name1', value: 2},
                        {id: 2, name:'name2', value: 2},
                        {id: 3, name:'name3', value: 2}
                    ]},
                    {id: 2, name:'name2', nick:'nick2', caracteristicas: [
                        {id: 0, name:'name0', value: 3},
                        {id: 1, name:'name1', value: 3},
                        {id: 2, name:'name2', value: 3},
                        {id: 3, name:'name3', value: 3}
                    ]},
                    {id: 3, name:'name3', nick:'nick3', caracteristicas: [
                        {id: 0, name:'name0', value: 4},
                        {id: 1, name:'name1', value: 4},
                        {id: 2, name:'name2', value: 4},
                        {id: 3, name:'name3', value: 4}
                    ]},
            ];
        });
        it("drops an error when has no valid caracteristicas values", function(){
            item = {id: 0, name: 'name0'};
            spyOn(calc, 'hasNoValidChildCollectionValues').andCallThrough();
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeDefined();
            });
            expect(calc.hasNoValidChildCollectionValues).toHaveBeenCalled();
        });
        it("drops an error when has no lesion field", function(){
            item.lesion = undefined;
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeDefined();
            });
        });
        it("has valid caracteristicas when they sum 100", function(done){
            spyOn(calc, 'hasNoValidChildCollectionValues').andCallThrough();
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeUndefined();
                done();
            });
            expect(calc.hasNoValidChildCollectionValues).toHaveBeenCalled();
        });
        it("retrieves fichas from repo given grupo id", function(done){
            spyOn(calc.repo, 'getCollectionItem').andCallFake(function(collectionName, id, callback){
                expect(collectionName).toBe('grupos');
                expect(id).toBe(0);
                callback(JSON.stringify({fichas: fichas}));
            });
            spyOn(calc.repo, 'getCollectionByFilter').andCallFake(function(collectionName, filter, callback){
                expect(collectionName).toBe('fichas');
                expect(filter).toBeDefined();
                callback(JSON.stringify(fichas));
            });
            spyOn(calc, 'getFichasByGroupName').andCallThrough();
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeUndefined();
                done();
            });
            expect(calc.getFichasByGroupName).toHaveBeenCalled();
        });
        it("calculates results of each ficha", function(done){
            spyOn(calc.repo, 'getCollectionItem').andCallFake(function(collectionName, id, callback){
                callback(JSON.stringify({fichas: fichas}));
            });
            spyOn(calc.repo, 'getCollectionByFilter').andCallFake(function(collectionName, filter, callback){
                callback(JSON.stringify(fichas));
            });
            spyOn(calc, 'calculateFicha').andCallThrough();
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeUndefined();
                expect(calculatedItem.results.length).toBe(4);
                done();
            });
            expect(calc.calculateFicha.calls.length).toBe(4);
        });
        it("retrieves results", function(done){
            spyOn(calc.repo, 'getCollectionItem').andCallFake(function(collectionName, id, callback){
                callback(JSON.stringify({fichas: fichas}));
            });
            spyOn(calc.repo, 'getCollectionByFilter').andCallFake(function(collectionName, filter, callback){
                callback(JSON.stringify(fichas));
            });
            spyOn(calc, 'getLesionIndexArray').andCallFake(function(fichasLength, numLesionados){
                return [1];
            });
            spyOn(calc, 'orderResultsDesc').andCallThrough();
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeUndefined();
                expect(calculatedItem.date_finish).toBeDefined();
                checkResults(calculatedItem.results);
                done();
            });
            expect(calc.getLesionIndexArray).toHaveBeenCalled();
            expect(calc.orderResultsDesc).toHaveBeenCalled();
        });
        it("retrieves lesionados", function(done){
            spyOn(calc.repo, 'getCollectionItem').andCallFake(function(collectionName, id, callback){
                callback(JSON.stringify({fichas: fichas}));
            });
            spyOn(calc.repo, 'getCollectionByFilter').andCallFake(function(collectionName, filter, callback){
                callback(JSON.stringify(fichas));
            });
            spyOn(calc, 'getLesionIndexArray').andCallFake(function(fichasLength, numLesionados){
                expect(fichasLength).toBe(fichas.length);
                expect(numLesionados).toBe(Math.round(fichas.length * (item.lesion/100)));
                return [1];
            });
            calc.calculateResults(item, function(calculatedItem){
                expect(calculatedItem.error).toBeUndefined();
                done();
            });
            expect(calc.getLesionIndexArray).toHaveBeenCalled();
        });
        function checkResults(results){
            expect(results.length).toBe(fichas.length);
            expect(results[0].ficha.nick).toBe('nick3');
            expect(results[0].lesion).toBe(undefined);
            expect(results[0].resultado).toBe(400);
            expect(results[1].resultado).toBe(300);
            expect(results[2].resultado).toBe(100);
            expect(results[3].resultado).toBe(0);
            expect(results[3].lesion).not.toBe(undefined);
            expect(results[3].ficha.nick).toBe('nick1');
        }
    });
})();