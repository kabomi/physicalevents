/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

"use strict";

(function(){

    var PORT = 3000;
    var SERVER = "localhost";
    var SERVER_URI = "http://" + SERVER + ":" + PORT + "/";


    var server;
    var fs = require("fs");
    var util = require("../../lib/server_lib");
    var httpGet_lib = require("../../lib/spec_lib").httpGet;
    var httpPost_lib = require("../../lib/spec_lib").httpPost;
    var httpRequest_lib = require("../../lib/spec_lib").httpRequest;
    var repo = require("../mongodb_repo").init("berlin-test");

    describe("Handlers", function(){
        var expectedData = "Hello world";

        beforeEach(function(){
            server = require("../server").init({
                port: PORT,
                repo: repo});
        });
        afterEach(function(done){
            if(server){
                try{
                    server.stop(function(){
                        done();
                    });
                }catch(ex){
                    done();
                }
            }
        });

        describe("Login", function(){
            it("response a json with user information", function(done){
                var url = SERVER_URI + "login";
                var TEST_USER = 'test';
                var TEST_PASS = 'test';

                httpPost({user: TEST_USER, pass: TEST_PASS}, function(response, responseData){
                    expect(response.statusCode).toBe(200);
                    expect(JSON.parse(responseData).user).toBe(TEST_USER);
                    done();
                });
            });
        });

        describe("Serve Data", function(){
            var collectionName = 'fichas';
            var expectedData;
            beforeEach(function(){
                spyOn(util, 'decodeUri').andCallThrough();
            });
            it("serves json object when collection is requested", function(done){
                var url = SERVER_URI + "data/" + collectionName;
                expectedData = "[{id: 0, name: test}]";

                spyOn(server.repo, 'getCollection').andCallFake(function(type, callback){
                    callback(expectedData);
                });

                httpGet(url, function(response, responseData){
                    expect(util.decodeUri).toHaveBeenCalled();
                    testGetJsonData(response, responseData, expectedData, done);
                });
            });
            it("serves json object when item from a collection is requested", function(done){
                var itemId = '0';
                var url = SERVER_URI + "data/" + collectionName + '/' + itemId;
                expectedData = '{"id":"0","name":"test"}';

                spyOn(server.repo, 'getCollection').andCallFake(function(collectionName, callback){
                    callback('[]');
                });
                spyOn(server.repo, 'getCollectionItem').andCallFake(function(collectionName, itemId, callback){
                    callback(expectedData);
                });
                httpGet(url, function(response, responseData){
                    expect(util.decodeUri).toHaveBeenCalled();
                    expect(server.repo.getCollectionItem.calls[0].args[0]).toBe(collectionName);
                    expect(server.repo.getCollectionItem.calls[0].args[1]).toBe(itemId);
                    testGetJsonData(response, responseData, expectedData, done);
                });
            });
            it("serves json object when item collection requested with child collections data updated", function(done){
                var itemId = '0';
                var url = SERVER_URI + "data/" + collectionName + '/' + itemId;
                var testItem = '{' +
                    '"_id":"531afbc3d0cd9c509dd555b0",' +
                    '"id":"531afbc3d0cd9c509dd555b0",' +
                    '"name":"evento0",' +
                    '"caracteristicas":[' +
                        '{"name":"car2"},' +
                        '{"_id":"531afacfd0cd9c509dd555a7","id":"531afacfd0cd9c509dd555a7","name":"car0"},' +
                        '{"_id":"531afacfd0cd9c509dd555a9","id":"531afacfd0cd9c509dd555a9","name":"car2","value":40}]' +
                    '}';
                var testChildCollection = '[' +
                    '{"_id":"531afacfd0cd9c509dd555a9","id":"531afacfd0cd9c509dd555a9","name":"car2-renamed"}' +
                ']';
                expectedData = '{' +
                    '"_id":"531afbc3d0cd9c509dd555b0",' +
                    '"id":"531afbc3d0cd9c509dd555b0",' +
                    '"name":"evento0",' +
                    '"caracteristicas":[{"_id":"531afacfd0cd9c509dd555a9","id":"531afacfd0cd9c509dd555a9","name":"car2-renamed","value":40}]' +
                '}';

                spyOn(server.repo, 'getCollectionItem').andCallFake(function(collectionName, itemId, callback){
                    callback(testItem);
                });
                spyOn(server.repo, 'getCollection').andCallFake(function(collectionName, callback){
                    callback(testChildCollection);
                });
                httpGet(url, function(response, responseData){
                    expect(util.decodeUri).toHaveBeenCalled();
                    expect(server.repo.getCollectionItem).toHaveBeenCalled();
                    expect(server.repo.getCollection).toHaveBeenCalled();
                    testGetJsonData(response, responseData, expectedData, done);
                });
            });
            it("serves json object when item results from a collection are requested", function(done){
                var itemId = '0';
                var url = SERVER_URI + "data/" + collectionName + '/' + itemId + '/clasificacion';
                var testData = {"id":"0","name":"test"};
                var testLesionesData = [{"id":"0","name":"test","description":"testDescription"}];
                expectedData = JSON.stringify(testData);
                var lesionesTestData = JSON.stringify(testLesionesData);

                spyOn(server.repo, 'getCollection').andCallFake(function(collectionName, callback){
                    var newExpectedData = testData;
                    newExpectedData.lesiones = testLesionesData;
                    expectedData = JSON.stringify(newExpectedData);
                    callback(lesionesTestData);
                });
                spyOn(server.handlers, 'calculateResults').andCallFake(function(item, callback){
                    expect(item.lesiones).toBeDefined();
                    callback(item);
                });
                spyOn(server.repo, 'getCollectionItem').andCallFake(function(collectionName, itemId, callback){
                    callback(expectedData);
                });
                httpGet(url, function(response, responseData){
                    expect(util.decodeUri).toHaveBeenCalled();
                    expect(server.repo.getCollectionItem.calls[0].args[0]).toBe(collectionName);
                    expect(server.repo.getCollectionItem.calls[0].args[1]).toBe(itemId);
                    expect(server.handlers.calculateResults).toHaveBeenCalled();
                    testGetJsonData(response, responseData, expectedData, done);
                });
            });
        });
        describe("Modify Data", function(){
            var collectionName = 'fichas';
            var path;
            var expectedData, options;
            beforeEach(function(){
                path = "/data/" + collectionName + '/' + 'new';
                expectedData = {id: 500, name: 'test'};
                options = {
                    path: path,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                spyOn(util, 'decodeUri').andCallThrough();
            });

            it("uses express bodyParser", function(done){
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, postData, callback){
                    expect(postData).toBeDefined();
                    callback();
                });
                httpRequest(options, expectedData, function(response, responseData){
                    done();
                });
            });
            it("creates a collection item when requested", function(done){
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, postData, callback){
                    callback(JSON.stringify(postData));
                });
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(server.repo.setCollectionItem.calls[0].args[0]).toBe(collectionName);
                        expect(server.repo.setCollectionItem.calls[0].args[1].name).toBe(expectedData.name);
                        testGetJsonData(response, responseData, JSON.stringify(expectedData), done);
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("updates a collection item when requested", function(done){
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, postData, callback){
                    callback(JSON.stringify(postData));
                });
                options.path = "/data/" + collectionName + '/' + expectedData.name;
                options.method = "PUT";
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem.calls[0].args[0]).toBe(collectionName);
                        expect(server.repo.setCollectionItem.calls[0].args[1].name).toBe(expectedData.name);
                        testGetJsonData(response, responseData, JSON.stringify(expectedData), done);
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("updates grupos collection when a fichas collection item update is requested", function(done){
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, postData, callback){
                    callback(JSON.stringify(postData));
                });
                spyOn(server.handlers, 'onFichasUpdate').andCallFake(function(ficha, callback){
                    callback();
                });
                options.path = "/data/" + 'fichas' + '/' + expectedData.name;
                options.method = "PUT";
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(server.handlers.onFichasUpdate).toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });

            it("doesnt insert a collection item when its name is ''", function(done){
                spyOn(server.repo, 'setCollectionItem');
                options.path = "/data/" + collectionName + '/' + 'new';
                options.method = "POST";
                httpRequest(options, {name: ''}, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("doesnt insert a collection item when its name is 'new'", function(done){
                spyOn(server.repo, 'setCollectionItem');
                options.path = "/data/" + collectionName + '/' + 'new';
                options.method = "POST";
                httpRequest(options, {name: 'new'}, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("doesnt insert a collection item when its name is taken", function(done){
                var testData = [{id:0, name:'test0'},
                    {id:1, name:'test1'}];
                var insertData = {name:'test1'};
                spyOn(server.repo, 'getCollectionItem').andCallFake(function(collectionName, itemValue, callback){
                    callback(JSON.stringify(testData[0]));
                });
                spyOn(server.repo, 'getCollection').andCallFake(function(collectionName, callback){
                    callback(JSON.stringify(testData));
                });
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, item, callback){
                    callback(true);
                });
                options.path = "/data/" + collectionName + '/' + 'new';
                options.method = "POST";
                httpRequest(options, insertData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.getCollection).toHaveBeenCalled();
                        expect(server.repo.getCollectionItem).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });

            it("doesnt update a collection item when its name is ''", function(done){
                spyOn(server.repo, 'setCollectionItem');
                options.path = "/data/" + collectionName + '/' + '';
                options.method = "PUT";
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("doesnt update a collection item when its name is 'new'", function(done){
                spyOn(server.repo, 'setCollectionItem');
                options.path = "/data/" + collectionName + '/' + 'new';
                options.method = "PUT";
                expectedData.name = 'new';
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("doesnt update a collection item when its name is not taken", function(done){
                var testData = [{id:0, name:'test0'},
                    {id:1, name:'test1'}];
                var updatedData = {id:0, name:'test1'};
                spyOn(server.repo, 'getCollectionItem').andCallFake(function(collectionName, itemValue, callback){
                    callback(JSON.stringify(testData[0]));
                });
                spyOn(server.repo, 'getCollection').andCallFake(function(collectionName, callback){
                    callback(JSON.stringify(testData));
                });
                spyOn(server.repo, 'setCollectionItem').andCallFake(function(collectionName, item, callback){
                    callback(true);
                });
                options.path = "/data/" + collectionName + '/' + 0;
                options.method = "PUT";
                httpRequest(options, updatedData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.getCollection).toHaveBeenCalled();
                        expect(server.repo.getCollectionItem).toHaveBeenCalled();
                        expect(server.repo.setCollectionItem).not.toHaveBeenCalled();
                        done();
                    }catch(ex){
                        done(ex.message);
                    }
                });
            });
            it("deletes a collection item when requested given item's id value", function(done){
                var itemValue = expectedData.id + '';
                checkDeleteCollectionItemGivenItemValue(itemValue, done);
            });
            it("deletes a collection item when requested given item's name value", function(done){
                var itemValue = expectedData.name + '';
                checkDeleteCollectionItemGivenItemValue(itemValue, done);
            });
            function checkDeleteCollectionItemGivenItemValue(itemValue, done){
                options.path = "/data/" + collectionName + '/' + itemValue;
                options.method = 'DELETE';
                spyOn(server.repo, 'removeCollectionItem').andCallFake(function(collectionName, expectedData, callback){
                    callback(JSON.stringify({deleted:true}));
                });
                httpRequest(options, expectedData, function(response, responseData){
                    try{
                        expect(util.decodeUri).toHaveBeenCalled();
                        expect(server.repo.removeCollectionItem.calls[0].args[0]).toBe(collectionName);
                        expect(server.repo.removeCollectionItem.calls[0].args[1]).toBe(itemValue);
                        testGetJsonData(response, responseData, JSON.stringify({deleted:true}), done);
                    }catch(ex){
                        done(ex.message);
                    }
                });
            }
        });

        function testGetJsonData(response, responseData, expectedData, done){
            expect(response.statusCode).toBe(200);
            expect(response.headers['content-type']).toBe("application/json");
            expect(responseData).toBe(expectedData);
            done();
        }

        function httpGet(url, callback){
            server.start(
                function(){
                    httpGet_lib(url, callback);
                }
            );
        }
        function httpPost(data, callback){
            server.start(
                function(){
                    httpPost_lib(data, callback);
                }
            );
        }
        function httpRequest(options, data, callback){
            var innerOptions = {
                hostname: options.hostname || SERVER,
                port: options.port || PORT,
                path: options.path,
                method: options.method,
                headers: options.headers || {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            server.start(
                function(){
                    httpRequest_lib(innerOptions, data, callback);
                }
            );
        }
    });
})();