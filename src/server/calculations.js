/* jshint -W003 */

"use strict";

(function(){

    var util = require("../lib/server_lib");
    var self = {};

    function calculateResults(item, callback){

        if (hasNoValid(item, throwError)) return;

        self.getFichasByGroupName(item.grupo, function(fichas){
            if (util.isNotArray(fichas)){
                callback(fichas);
                return;
            }

            var results = [];
            var lesionValue = parseInt(item.lesion);
            var lesionados = Math.round(fichas.length * (lesionValue/100));
            var lesionIndexArray = self.getLesionIndexArray(fichas.length, lesionados);
            fichas.forEach(function(ficha, index){
                var isLesionado = (lesionIndexArray.indexOf(index) >= 0);
                var calculatedFicha = self.calculateFicha(item.caracteristicas, ficha, isLesionado);
                if (isLesionado){
                    var randomIndex = Math.floor((Math.random()*(item.lesiones.length)));
                    calculatedFicha.lesion = item.lesiones[randomIndex];
                }
                results.push(calculatedFicha);
            });
            self.orderResultsDesc(results);
            item.results = results;
            item.date_finish = util.now();
            callback(item);
        });

        function throwError(err){
            callback(err);
        }
    }
    function hasNoValid(item, callback){
        var debugInfo = "item:" + JSON.stringify(item);
        if (self.hasNoValidChildCollectionValues(item)){
            callback({error: 'Invalid child collection values', debug: debugInfo});
            return true;
        }
        if (isValidLesion(item.lesion)){
            callback({error: 'Invalid lesion value', debug: debugInfo});
            return true;
        }
        return false;
    }
    function isValidLesion(value){
        var lesionValue = parseInt(value);
        return (!util.isNumber(lesionValue) ||
                    lesionValue > 100 ||
                    lesionValue < 0);
    }
    function hasNoValidChildCollectionValues(item){
        var childCollection = item.caracteristicas;
        if (util.isNotArray(childCollection)) return true;

        var sum = 0;
        childCollection.forEach(function(item){
            var value = parseInt(item.value);
            if (value > 0) sum = sum + value;
        });
        return (sum !== 100);
    }
    function getFichasByGroupName(id, callback){
        self.repo.getCollectionItem('grupos', id, function(jsonResult){
            var fichasChildCollection = JSON.parse(jsonResult).fichas;
            if (util.isArray(fichasChildCollection)){
                var filterArray = [];
                fichasChildCollection.forEach(function(ficha){
                    filterArray.push(ficha.id);
                });
                var filter = {
                    id: { $in:  filterArray}
                };
                console.log("filter:" + JSON.stringify(filter));
                self.repo.getCollectionByFilter('fichas', filter, function(jsonResult){
                    var fichas = JSON.parse(jsonResult);
                    console.log("fichas:" + jsonResult);
                    if (util.isArray(fichas)){
                        util.printObj(fichas, "fichas");
                        callback(fichas);
                        return;
                    }
                    callback({error: 'Filtered grupo id fichas returned empty fichas collection'});
                });
                return;
            }
            callback({error: 'Grupo id returned empty fichas collection'});
        });
        
    }
    function getLesionIndexArray(count, numLesionados){
        var lesionArray = [];
        if (numLesionados > count) throw {error: 'Num lesionados > count'};

        while (lesionArray.length < numLesionados){
            var randomIndex = Math.floor((Math.random()*(count)));
            if (lesionArray.indexOf(randomIndex) < 0){
                lesionArray.push(randomIndex);
            }
        }
        return lesionArray;
    }
    function calculateFicha(caracteristicas, ficha, isLesionado){
        var resultado = 0;
        if (!isLesionado) {
            caracteristicas.forEach(function(car){
                var fichaCar = getFichaCar(ficha, car);
                if (car.value > 0 &&
                    fichaCar !== undefined &&
                    fichaCar.value > 0){

                    resultado = resultado + (car.value * fichaCar.value);
                }
            });
        }
        
        return {
            ficha: ficha,
            resultado: resultado
        };
    }
    function getFichaCar(ficha, car){
        var  selectedFichaCar;
        if (util.isNotArray(ficha.caracteristicas)) return selectedFichaCar;

        ficha.caracteristicas.forEach(function(fichaCar){
            if (fichaCar.name === car.name) selectedFichaCar = fichaCar;
        });

        return selectedFichaCar;
    }
    function orderResultsDesc(collection){
        if (util.isNotArray(collection)) return;
        
        collection.sort(function(a, b){
            return (b.resultado - a.resultado);
        });
    }

    function init(options){
        self.repo = options.repo;

        self.calculateResults = calculateResults;
        self.hasNoValidChildCollectionValues = hasNoValidChildCollectionValues;
        self.getFichasByGroupName = getFichasByGroupName;
        self.getLesionIndexArray = getLesionIndexArray;
        self.calculateFicha = calculateFicha;
        self.orderResultsDesc = orderResultsDesc;

        return self;
    }

    exports.init = init;
    
})();