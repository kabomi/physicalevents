
(function(){

    "use strict";
    var port = parseInt(process.argv[2]);
    var contentDir = "src/server/content/";
    var clientAppDir = "src/client/";
    var assetsDir = "src/client/assets/";
    var dbName = "berlin";
    var server = require("./server").init({
        port: port,
        mainFile: contentDir + 'index.html',
        notFoundFile: contentDir + '404.html',
        assetsDir: assetsDir,
        clientAppDir: clientAppDir,
        repo: require("./mongodb_repo").init(dbName)});

    server.start(function(){
        console.log("Server started");
    });
}());