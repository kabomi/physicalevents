/* jshint -W083 */

var berlin = berlin || {};
berlin.Interactors = berlin.Interactors || {};

(function(){
    "use strict";

    function collectionInteractor(name, app){
        var self = {};
        var collectionName = name;
        var Factory = berlin.Factory;
        var util = berlin.Util;

        self.repo = app.repo;
        self.initButtons = app.initButtons;
        self.collectionPanels = app.collectionPanels;
        self.backPanel = app.backPanel;
        self.actionPanel = app.actionPanel;
        self.resultsPanel = app.resultsPanel;
        self.editPanel = Factory.createPanel(name + '-edit');
        self.addToCollectionPanel = Factory.createPanel(name + '-addToCollection');
        self.handlers = {
            show:  function(){   onShow();},
            new:   function(){   onNew();},
            edit:  function(item){
                var decodeItem = util.decodeUri(item);
                onEdit(decodeItem);},
            delete:function(item){
                var decodeItem = util.decodeUri(item);
                onDelete(decodeItem);},
            addToCollection:function(item){
                var decodeItem = util.decodeUri(item);
                onEdit(decodeItem, onAddToCollection);}
        };
        self.routes = {};
        self.history = app.history;

        function initialize(){
            self.addRoutes();
            self.initButtons[collectionName].onClick =  onShowCollectionClick;
        }
        function onShowCollectionClick(){
            self.handlers.show();
            self.history.pushState('/' + collectionName, collectionName.toUpperCase());
        }
        function onNewButtonClick(){
            self.handlers.new();
            self.history.pushState('/' + collectionName + '/new', 'NUEVO');
        }
        function onEditClick(item){
            self.handlers.edit(item);
            self.history.pushState('/' + collectionName + '/' + item.name, 'EDITAR');
        }
        function onDeleteClick(item){
            self.handlers.delete(item);
        }
        function onAddToCollectionClick(item){
            onAddToCollection(item);
            var itemName;
            if (typeof(item) === 'object' &&
                item !== null){

                itemName = item.name;
            }else{
                itemName = item;
            }
            self.history.pushState('/' + collectionName + '/' + itemName + '/' + self.childCollectionName, self.childCollectionName.toUpperCase());
        }
        function resolveEditView(){
            self.initButtons.hide();
            self.actionPanel.hide();
            self.addToCollectionPanel.hide();
            self.collectionPanels.hide();
            self.resultsPanel.hide();
            self.editPanel.show();
        }
        function resolveShowView(){
            self.initButtons.hide();
            self.collectionPanels.hide();
            self.editPanel.hide();
            self.addToCollectionPanel.hide();
            self.resultsPanel.hide();
            self.collectionPanels[collectionName].show();
            self.backPanel.show();
            self.actionPanel.show();
        }
        function resolveNewView(){
            self.actionPanel.show();
        }
        function onShow(){
            resolveShowView();
            self.newButton = Factory.createNewButton();
            self.newButton.onClick = onNewButtonClick;
            self.paintInteractor.paintCollection();
        }
        function onNew(){
            onEdit({});
            resolveNewView();
        }
        function onEdit(item, callback){
            resolveEditView();
            self.repo.getCollectionItem(collectionName, item, function(uri, jsonResult){
                if (self.hasChildCollection()){
                    self.addToCollectionButton = Factory.createAddToCollectionButton(collectionName, jsonResult);
                    self.addToCollectionButton.onClick = onAddToCollectionClick;
                }
                self.paintInteractor.paintCollectionItem(jsonResult);

                if (callback) callback(jsonResult);
            });
        }
        function onDelete(item){
            var options = {text: "&iquest;Seguro que quieres borrar el item (" + item.name + ")?",
                    title: "ELIMINAR"};
            berlin.Util.prompt(options, function(e,v,m,f){
                if (v){
                    self.repo.deleteCollectionItem(collectionName, item, function(uri, response){
                        if (response || response.deleted){
                            util.log("Elemento borrado: " + item.name);
                        }else{
                            util.log("No se pudo borrar: " + item.name);
                        }
                        onShow();//TODO:test
                    });
                }
            });
        }
        function onAddToCollection(item){
            self.addToCollectionPanel.show();
            self.editPanel.hide();
            self.backPanel.show();
            self.repo.getCollection(self.childCollectionName, function(uri, response){
                if (response){
                    self.paintInteractor.paintCollectionChildWithValue(response, item);
                }
            });
        }
        
        function hasChildCollection(){
            return (self.childCollectionName !== undefined &&
                    self.childCollectionName !== null);
        }

        function onItemChange(item, objectValue){
            var itemHasChanged = false;
            var validName = true;
            var settingNewItem = false;
            //util.printObj(objectValue, "onItemChange");
            for (var name in objectValue){
                if (objectValue.hasOwnProperty(name)){
                    if (name !== 'id' &&
                        name !== '_id'){
                        var value = objectValue[name];
                        if (name === 'name' &&
                            checkInvalidName(item.name, value)){
                            validName = false;
                        }else{
                            if(name === 'name'){
                                settingNewItem = true;
                            }
                            var trimmedValue = util.getValueTrimmed(value);
                            item[name] = trimmedValue;
                            util.log("new value for " + name + ":" + value);
                            itemHasChanged = true;
                        }
                    }
                }
            }
            if (item.name === undefined){
                util.log("El elemento no tiene un campo 'name'");
                return;
            }
            if (itemHasChanged && validName){
                self.repo.updateCollectionItem(collectionName, item, function(uri, response){
                    if (response){
                        self.history.replaceState('/' + collectionName + '/' + item.name, item.name.toUpperCase());
                        util.savedData("Elemento modificado: " + item.name);
                        if (settingNewItem &&
                            self.paintInteractor.imageForm){
                            util.setImageForm(response.id, self.paintInteractor.imageForm);
                        }
                    }else{
                        util.notSavedData("No se pudo modificar: " + item.name);
                    }
                });
            }
        }
        function checkInvalidName(oldValue, newValue){
            if (newValue.toLowerCase() === 'new' ||
                newValue === ''){
                util.alert('No se puede utilizar el nombre ' + newValue);
                return true;
            }
            var collection = self.repo.collections[collectionName];
            if (util.isNotArray(collection)) return false;

            var invalidNameFound = false;
            collection.forEach(function(item){
                var itemName = item.name.toLowerCase();
                if(itemName === newValue.toLowerCase() &&
                    itemName !== oldValue.toLowerCase()){
                    invalidNameFound = true;
                }
            });
            if (invalidNameFound){
                util.alert('No se puede utilizar el nombre ' + newValue + ' porque ya esta siendo utilizado');
            }
            return invalidNameFound;
        }

        function getTableActions(){
            var actions = {
                EDITAR: onEditClick,
                BORRAR: onDeleteClick
            };

            return actions;
        }


        function addRoutes(){
            self.routes[collectionName] =  crossroads.addRoute('/' + collectionName,  self.handlers.show);
            self.routes.new = crossroads.addRoute('/' + collectionName + '/new', self.handlers.new);
            self.routes.edit = crossroads.addRoute('/' + collectionName + '/{id}', self.handlers.edit);
            self.routes.delete = crossroads.addRoute('/' + collectionName + '/{id}/delete', self.handlers.delete);
            self.routes.addToCollection = crossroads.addRoute('/' + collectionName + '/{id}/' + self.childCollectionName, self.handlers.addToCollection);
        }


        self.initialize = initialize;
        self.getCollectionName = function(){ return collectionName};
        self.getTableActions = getTableActions;
        self.onItemChange = onItemChange;
        self.hasChildCollection = hasChildCollection;
        self.addRoutes = addRoutes;
        self.onEdit = onEdit;

        return self;
    }


    berlin.Interactors.collectionInteractor = collectionInteractor;
})(berlin);