
var berlin = berlin || {};

(function(){
    "use strict";

    var util = berlin.Util;
    function Factory(){
        var self = this;
        var collectionNames = ['grupos', 'fichas', 'eventos', 'caracteristicas', 'lesiones'];
        var collectionNamesWithChildCollections = ['grupos', 'fichas', 'eventos'];
        var collectionFieldNames = {
            grupos: ['name'],
            fichas: ['name', 'nick', 'grupo'],
            eventos:['name', 'grupo'],
            caracteristicas:['name', 'alias'],
            lesiones: ['name', 'description']
        };
        var collectionFieldCaptions = {
            grupos: ['Nombre'],
            fichas: ['Nombre', 'Alias', 'Grupo'],
            eventos:['Nombre', 'Grupo'],
            caracteristicas:['Nombre', 'Alias'],
            lesiones: ['Nombre', 'Descripci&oacute;n']
        };
        self.collectionNames = collectionNames;
        self.collectionNamesWithChildCollections = collectionNamesWithChildCollections;

        function getCollectionTableFieldNames(collectionName){
            return collectionFieldNames[collectionName];
        }
        function getCollectionTableCaptionNames(collectionName){
            return collectionFieldCaptions[collectionName];
        }
        function createLogin(loginId){
            var self = {};
            var _user, _pass;

            self.setUser = function(userName){
                _user = userName;
            };
            self.setPass = function(password){
                _pass = password;
            };

            self.logIn = function(callback, errorCallback){
                util.ajaxPost('/login',
                    {
                        user: _user,
                        pass: _pass
                    },
                    function(data, textStatus, response){
                        util.log("LOGIN SUCCEDED!");
                        callback(response);
                    },
                    function(error){
                        util.log("LOGIN ERROR!");
                        errorCallback(error);
                    }
                );
            };

            self.nativeWidget = attachNativeWidget("login-box-button", "button");
            return self;
        }
       
        function createObject(id, tag){
            var self = {};
            self.nativeWidget = attachNativeWidget(id, tag);

            self.show = function(){
                self.nativeWidget.show();
            };
            self.hide = function(){
                self.nativeWidget.hide();
            };
            self.empty = function(){
                self.nativeWidget.empty();
            };
            self.getValue = function(){
                return self.nativeWidget.val();
            };
            self.setValue = function(value){
                return self.nativeWidget.val(value);
            };
            self.addClass = function(value){
                self.nativeWidget.addClass(value);
            };

            self.onClick = function(){/*event*/};

            self.bind = function(eventType, withFunction){
                self.nativeWidget.on(eventType, withFunction);
            };
            self.unbind = function(eventType){
                self.nativeWidget.unbind(eventType);
            };

            return self;
        }
        function attachNativeWidget(id, tag){
            return util.createDomControlIfNotExists(id, tag);
        }

        function createInitButtons(){
            var self = {};
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                self[name] = createButton(name);
            });
            self.show = function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    self[name].show();
                });
            };
            self.hide = function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    self[name].hide();
                });
            };

            return self;
        }
        function createButton(type, param){
            var self = createObject(type + '-button', 'button');
            self.unbind('click');
            self.bind('click', function(){self.onClick(param);});

            return self;
        }
        function createInputText(type){
            var self = createObject(type + '-input', 'input');
            return self;
        }
        function createList(type){
            var self = createObject(type + '-ul', "ul");
            return self;
        }
        function createSelect(type){
            var self = createObject(type + '-select', "select");
            return self;
        }
        function createCollectionPanels(){
            var self = {};
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                self[name] = createPanel(name);
                self[name + '-edit'] = createPanel(name + '-edit');
                self[name + '-addToCollection'] = createPanel(name + '-addToCollection');
            });
            self.hide = function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    self[name].hide();
                    self[name + '-edit'].hide();
                    self[name + '-addToCollection'].hide();
                });
            };

            return self;
        }
        function createPanel(type){
            var self = createObject(type + '-panel', "div");
            return self;
        }
        function createTable(type){
            var self = createObject(type + '-table', "table");
            return self;
        }
        function createImage(type){
            var self = createObject(type + '-image', "image");
            return self;
        }
        function createForm(options){
            var self = createObject(options.id + '-form', "form");
            self.nativeWidget.prop('action', options.action);
            self.nativeWidget.prop('enctype', "multipart/form-data");

            return self;
        }


        function createBackPanel(){
            var self = createPanel('back');
            return self;
        }

        function createBackButton(){
            var self = createButton('back');
            return self;
        }

        function createHomeButton(){
            var self = createButton('home');
            return self;
        }

        function createActionPanel(){
            var self = createPanel('action');
            return self;
        }

        function createResultsPanel(){
            var self = createPanel('results');
            return self;
        }

        function createNewButton(){
            var self = createButton('new');
            return self;
        }
        function createAddToCollectionButton(collectionName, param){
            var self = createButton(collectionName + '-addToCollection', param);
            return self;
        }

        function createHistory(){
            var self = {};

            function pushState(url, title, data){
                history.pushState(data, title, url);
            }
            function popState(){
                history.back();
            }
            function replaceState(url, title, data){
                history.replaceState(data, title, url);
            }

            self.pushState = pushState;
            self.popState =  popState;
            self.replaceState =  replaceState;
            
            return self;
        }

        function createRepo(path){
            var repo = new berlin.Repository(path);
            return repo;
        }

        function createCollectionInteractor(collectionName, app){
            var self = new berlin.Interactors.collectionInteractor(collectionName, app);
            self.paintInteractor = new berlin.Interactors.paintInteractor(self);

            return self;
        }
        function createEventosCollectionInteractor(collectionName, app){
            var self = new berlin.Interactors.eventosCollectionInteractor(collectionName, app);
            self.paintInteractor = new berlin.Interactors.eventosPaintInteractor(self);

            return self;   
        }
        function createFichasCollectionInteractor(collectionName, app){
            var self = new berlin.Interactors.collectionInteractor(collectionName, app);
            self.paintInteractor =  new berlin.Interactors.fichasPaintInteractor(self);
            
            return self;
        }
        function createGruposInteractor(app){
            var collectionName = 'grupos';
            var self = createCollectionInteractor(collectionName, app);

            self.editTags = {
                name: {
                    id: collectionName + '-name',
                    type: 'input',
                    name: 'name'
                },
                fichas: {
                    id: collectionName + '-fichas',
                    type: 'ul',
                    name: 'fichas',
                    noInput: true
                }
            };
            self.childCollectionName = 'fichas';
            self.childCollectionSelect = 'grupos';


            return self;
        }
        function createFichasInteractor(app){
            var collectionName = 'fichas';
            var self = createFichasCollectionInteractor(collectionName, app);

            self.editTags = {
                name: {
                    id: collectionName + '-name',
                    type: 'input',
                    name: 'name'
                },
                nick: {
                    id: collectionName + '-nick',
                    type: 'input',
                    name: 'nick'
                },
                grupo: {
                    id: collectionName + '-grupo',
                    type: 'select',
                    name: 'grupo',
                    collectionName: 'grupos'
                },
                caracteristicas: {
                    id: collectionName + '-caracteristicas',
                    type: 'ul',
                    name: 'caracteristicas'
                }
            };
            self.childCollectionName = 'caracteristicas';

            self.childCollectionSelect = 'eventos';
            self.childCollectionUlSelect = 'eventos';

            return self;
        }
        function createEventosInteractor(app){
            var collectionName = 'eventos';
            var self = createEventosCollectionInteractor(collectionName, app);

            self.editTags = {
                name: {
                    id: collectionName + '-name',
                    type: 'input',
                    name: 'name'
                },
                date: {
                    id: collectionName + '-date',
                    type: 'input',
                    name: 'date'
                },
                grupo: {
                    id: collectionName + '-grupo',
                    type: 'select',
                    name: 'grupo',
                    collectionName: 'grupos'
                },
                lesion: {
                    id: collectionName + '-lesion',
                    type: 'input',
                    name: 'lesion'
                },
                caracteristicas: {
                    id: collectionName + '-caracteristicas',
                    type: 'ul',
                    name: 'caracteristicas'
                }
            };
            self.childCollectionName = 'caracteristicas';
            self.childCollectionSelect = 'eventos';

            return self;
        }
        function createCaracteristicasInteractor(app){
            var collectionName = 'caracteristicas';
            var self = createCollectionInteractor(collectionName, app);

            self.editTags = {
                name: {
                    id: collectionName + '-name',
                    type: 'input',
                    name: 'name'
                },
                alias: {
                    id: collectionName + '-alias',
                    type: 'input',
                    name: 'alias'
                }
            };

            return self;
        }
        function createLesionesInteractor(app){
            var collectionName = 'lesiones';
            var self = createCollectionInteractor(collectionName, app);

            self.editTags = {
                name: {
                    id: collectionName + '-name',
                    type: 'input',
                    name: 'name'
                },
                description: {
                    id: collectionName + '-description',
                    type: 'input',
                    name: 'description'
                }
            };

            return self;
        }
        
        self.createLogin = createLogin;
        self.createInitButtons = createInitButtons;
        self.createCollectionPanels = createCollectionPanels;
        self.createBackPanel = createBackPanel;
        self.createAddToCollectionButton = createAddToCollectionButton;
        self.createBackButton = createBackButton;
        self.createHomeButton = createHomeButton;
        self.createActionPanel = createActionPanel;
        self.createResultsPanel = createResultsPanel;
        self.createNewButton = createNewButton;
        self.createHistory = createHistory;
        self.createRepo = createRepo;
        self.createTable = createTable;
        self.createImage = createImage;
        self.createForm = createForm;
        self.createPanel = createPanel;
        self.createInputText = createInputText;
        self.createList = createList;
        self.createSelect = createSelect;
        self.createButton = createButton;
        self.createGruposInteractor = createGruposInteractor;
        self.createFichasInteractor = createFichasInteractor;
        self.createEventosInteractor = createEventosInteractor;
        self.createCaracteristicasInteractor = createCaracteristicasInteractor;
        self.createLesionesInteractor = createLesionesInteractor;
        self.getCollectionTableFieldNames = getCollectionTableFieldNames;
        self.getCollectionTableCaptionNames = getCollectionTableCaptionNames;

        return self;
    }

    berlin.Factory = new Factory();
})(berlin);