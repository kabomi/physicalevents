/* jshint -W083 */

var berlin = berlin || {};
berlin.Interactors = berlin.Interactors || {};

(function(){
    "use strict";

    function collectionInteractor(name, app){
        var self = {};
        var collectionName = name;
        var Factory = berlin.Factory;
        var util = berlin.Util;

        self.repo = app.repo;
        self.initButtons = app.initButtons;
        self.collectionPanels = app.collectionPanels;
        self.backPanel = app.backPanel;
        self.actionPanel = app.actionPanel;
        self.resultsPanel = app.resultsPanel;
        self.editPanel = Factory.createPanel(name + '-edit');
        self.addToCollectionPanel = Factory.createPanel(name + '-addToCollection');
        self.handlers = {
            show:  function(){   onShow();},
            new:   function(){   onNew();},
            edit:  function(item){
                var decodeItem = util.decodeUri(item);
                onEdit(decodeItem);},
            delete:function(item){
                var decodeItem = util.decodeUri(item);
                onDelete(decodeItem);},
            addToCollection:function(item){
                var decodeItem = util.decodeUri(item);
                onEdit(decodeItem, onAddToCollection);},
            showResult: function(item){
                var decodeItem = util.decodeUri(item);
                onEdit(decodeItem, onShowResult);}
        };
        self.routes = {};
        self.history = app.history;

        function initialize(){
            addRoutes();
            self.initButtons[collectionName].onClick =  onShowCollectionClick;
        }
        function onShowCollectionClick(){
            self.handlers.show();
            self.history.pushState('/' + collectionName, collectionName.toUpperCase());
        }
        function onNewButtonClick(){
            self.handlers.new();
            self.history.pushState('/' + collectionName + '/new', 'NUEVO');
        }
        function onEditClick(item){
            self.handlers.edit(item);
            self.history.pushState('/' + collectionName + '/' + item.name, 'EDITAR');
        }
        function onDeleteClick(item){
            self.handlers.delete(item);
        }
        function onAddToCollectionClick(item){
            onAddToCollection(item);
            var itemName;
            if (typeof(item) === 'object' &&
                item !== null){

                itemName = item.name;
            }else{
                itemName = item;
            }
            self.history.pushState('/' + collectionName + '/' + itemName + '/' + self.childCollectionName, self.childCollectionName.toUpperCase());
        }
        function onShowResultClick(item){
            if (item !== null &&
                typeof(item) === 'object' &&
                util.isArray(item.results)){

                self.handlers.showResult(item);
                self.history.pushState('/' + collectionName + '/' + item.name + '/clasificacion', 'CLASIFICACION');
            }else{
                self.handlers.showResult(item);
                self.history.pushState('/' + collectionName + '/' + item.name + '/clasificacion', 'CLASIFICACION');
            }
        }
        function resolveEditView(){
            self.initButtons.hide();
            self.actionPanel.hide();
            self.addToCollectionPanel.hide();
            self.collectionPanels.hide();
            self.resultsPanel.hide();
            self.editPanel.show();
        }
        function resolveShowView(){
            self.initButtons.hide();
            self.collectionPanels.hide();
            self.editPanel.hide();
            self.addToCollectionPanel.hide();
            self.resultsPanel.hide();
            self.collectionPanels[collectionName].show();
            self.backPanel.show();
            self.actionPanel.show();
        }
        function resolveResultsView(){
            self.editPanel.hide();
            self.resultsPanel.show();
        }
        function resolveNewView(){
            self.actionPanel.show();
        }
        function onShow(){
            resolveShowView();
            self.newButton = Factory.createNewButton();
            self.newButton.onClick = onNewButtonClick;
            self.paintCollection();
        }
        function onNew(){
            onEdit({});
            resolveNewView();
        }
        function onEdit(item, callback){
            resolveEditView();
            self.repo.getCollectionItem(collectionName, item, function(uri, jsonResult){
                if (self.hasChildCollection()){
                    self.addToCollectionButton = Factory.createAddToCollectionButton(collectionName, jsonResult);
                    self.addToCollectionButton.onClick = onAddToCollectionClick;
                }
                self.paintCollectionItem(jsonResult);

                if (callback) callback(jsonResult);
            });
        }
        function onDelete(item){
            var options = {text: "&iquest;Seguro que quieres borrar el item (" + item.name + ")?",
                    title: "ELIMINAR"};
            berlin.Util.prompt(options, function(e,v,m,f){
                if (v){
                    self.repo.deleteCollectionItem(collectionName, item, function(uri, response){
                        if (response || response.deleted){
                            util.log("Elemento borrado: " + item.name);
                        }else{
                            util.log("No se pudo borrar: " + item.name);
                        }
                        onShow();//TODO:test
                    });
                }
            });
        }
        function onAddToCollection(item){
            self.addToCollectionPanel.show();
            self.editPanel.hide();
            self.backPanel.show();
            self.repo.getCollection(self.childCollectionName, function(uri, response){
                if (response){
                    self.paintCollectionChildWithValue(response, item);
                }
            });
        }
        function onShowResult(item){
            if (util.isDefined(item) &&
                self.childCollectionName){

                var hasResults = isAlreadyCalculated(item);
                var hasValidChildCollection = self.hasValidChildCollectionValues(item);

                if (!hasResults &&
                    !hasValidChildCollection){

                    util.alert("Para poder ejectuar la accion " + self.childCollectionName + " deben sumar 100 en total");
                    return;
                }
                if (item.lesion === undefined || item.lesion < 0){
                    util.alert("Para poder ejecutar la accion el valor de lesion debe estar comprendido entre 0 y 100");
                    return;
                }
                if (item.grupo === undefined){
                    util.alert("Para poder ejecutar la accion debe seleccionar un grupo");
                    return;
                }

                self.calculateResults(item, function(calculatedItem){
                    resolveResultsView();
                    self.paintResults(calculatedItem);
                });
            }
        }
        function hasChildCollection(){
            return (self.childCollectionName !== undefined &&
                    self.childCollectionName !== null);
        }
        function isAlreadyCalculated(item){
            if (util.isDefined(item) &&
                util.isArray(item.results)){

                return true;
            }
            return false;
        }
        function hasValidChildCollectionValues(item){
            if (util.isArray(item.results)) return true;
            var childCollection = item[self.childCollectionName];
            if (util.isNotArray(childCollection)) return false;

            var sum = 0;
            childCollection.forEach(function(item){
                if (item.value > 0) sum = sum + parseInt(item.value);
            });
            return (sum === 100);
        }
        function calculateResults(item, callback){
            if (isAlreadyCalculated(item)){
                callback(item);
                return;
            }
            self.repo.getResults(collectionName, item.id, function(uri, jsonResult){
                if (jsonResult){
                    callback(jsonResult);
                }
            });
        }
        function onItemChange(item, objectValue){
            var itemHasChanged = false;
            var validName = true;
            var settingNewItem = false;
            //util.printObj(objectValue, "onItemChange");
            for (var name in objectValue){
                if (objectValue.hasOwnProperty(name)){
                    if (name !== 'id' &&
                        name !== '_id'){
                        var value = objectValue[name];
                        if (name === 'name' &&
                            checkInvalidName(item.name, value)){
                            validName = false;
                        }else{
                            if(name === 'name'){
                                settingNewItem = true;
                            }
                            var trimmedValue = util.getValueTrimmed(value);
                            item[name] = trimmedValue;
                            util.log("new value for " + name + ":" + value);
                            itemHasChanged = true;
                        }
                    }
                }
            }
            if (item.name === undefined){
                util.log("El elemento no tiene un campo 'name'");
                return;
            }
            if (itemHasChanged && validName){
                self.repo.updateCollectionItem(collectionName, item, function(uri, response){
                    if (response){
                        self.history.replaceState('/' + collectionName + '/' + item.name, item.name.toUpperCase());
                        util.savedData("Elemento modificado: " + item.name);
                        if (settingNewItem &&
                            self.imageForm){
                            util.setImageForm(response.id, self.imageForm);
                        }
                    }else{
                        util.notSavedData("No se pudo modificar: " + item.name);
                    }
                });
            }
        }
        function checkInvalidName(oldValue, newValue){
            if (newValue.toLowerCase() === 'new' ||
                newValue === ''){
                util.alert('No se puede utilizar el nombre ' + newValue);
                return true;
            }
            var collection = self.repo.collections[collectionName];
            if (util.isNotArray(collection)) return false;

            var invalidNameFound = false;
            collection.forEach(function(item){
                var itemName = item.name.toLowerCase();
                if(itemName === newValue.toLowerCase() &&
                    itemName !== oldValue.toLowerCase()){
                    invalidNameFound = true;
                }
            });
            if (invalidNameFound){
                util.alert('No se puede utilizar el nombre ' + newValue + ' porque ya esta siendo utilizado');
            }
            return invalidNameFound;
        }

        function paintCollection(){
            self.repo.getCollection(collectionName, function(uri, collection){
                if (collection === null)
                    collection = [];
                self.buildTableFromCollection(collection);
            });
        }
        function buildTableFromCollection(array){
            var actions = {
                EDITAR: onEditClick,
                BORRAR: onDeleteClick
            };
            if (self.hasResults){
                actions.LANZAR = onShowResultClick;
            }
            util.buildCollectionTable(collectionName, array, actions);
        }
        function paintCollectionItem(item){
            if (self.imageForm){
                var itemId = (item?item.id: undefined);
                util.setImageForm(itemId, self.imageForm);
            }
            if (self.hasResults){
                util.setClassificationButton(collectionName, item);
            }

            if (item === null ||
                item === undefined) return;

            for (var name in self.editTags){
                if (self.editTags.hasOwnProperty(name)){
                    var tag = self.editTags[name];
                    var itemFieldValue = item[name];
                    util.setFieldFromTagWithValue(tag, itemFieldValue, function(value){
                        self.onItemChange(item, value);
                    });
                    self.fillControlOptions(tag, itemFieldValue);
                }
            }
            if (self.childCollectionUlSelect === undefined) return;
            self.repo.getCollection(self.childCollectionUlSelect, function(uri, collection){
                if (collection === null) collection = [];
                util.fillCollectionChildSelect(collectionName + '-' + self.childCollectionName + '-ul',
                        collection,
                        self.filterCollectionChild);
            });
        }
        function fillControlOptions(tag, value){
            if (tag.type === 'select'){
                self.repo.getCollection(tag.collectionName, function(uri, collection){
                    if (collection === null) collection = [];

                    var exclude = value;
                    util.setOptionsFromTagWithCollection(tag, collection, exclude);
                });
            }
        }
        function paintCollectionChildWithValue(collection, item){
            var options = {
                item: item,
                collection: collection,
                collectionName: collectionName,
                childCollectionName: self.childCollectionName
            };
            util.setChildCollectionControls(options, function(value){
                self.onItemChange(item, value);
            });
            if (self.childCollectionSelect === undefined) return;
            self.repo.getCollection(self.childCollectionSelect, function(uri, collection){
                if (collection === null) collection = [];
                util.fillCollectionChildSelect(collectionName + '-' + self.childCollectionName,
                        collection,
                        self.filterCollectionChild);
            });
        }
        function filterCollectionChild(itemName, callback){
            if (self.collectionName === 'grupos'){
                self.repo.getCollection(self.childCollectionName, function(uri, collection){
                    if (collection === null ||
                        util.isNotArray(collection[self.childCollectionName])){
                        return callback([]);
                    }
                    var filteredCollection = [];
                    collection.forEach(function(childCollectionItem){
                        if (childCollectionItem.grupo === itemName){
                            filteredCollection.push(childCollectionItem);
                        }
                    });

                    return callback(filteredCollection);
                });
            }else{
                self.repo.getCollectionItem(self.childCollectionSelect, itemName, function(uri, collectionItem){
                    if (collectionItem === null ||
                        util.isNotArray(collectionItem[self.childCollectionName])){
                        return callback([]);
                    }
                    var filteredCollection = [];
                    collectionItem[self.childCollectionName].forEach(function(childCollectionItem){
                        filteredCollection.push(childCollectionItem);
                    });

                    return callback(filteredCollection);
                });
            }
        }
        function paintResults(item){
            console.dir(item);
            if (util.isArray(item.results)){
                var panel = Factory.createPanel('results-body');
                panel.empty();

                item.results.forEach(function(result, index){
                    var isLesionado = (result.lesion !== undefined);
                    var div = $('<div>');

                    var spanPos = $('<span>');
                    spanPos.addClass('badge pull-left');
                    spanPos.css('background-color', 'transparent');

                    var imgPath = "diploma.png";
                    if (index === 0) imgPath = "Trophy-gold.png";
                    if (index === 1) imgPath = "Trophy-silver.png";
                    if (index === 2) imgPath = "Trophy-bronze.png";
                    if (isLesionado) imgPath = "medic.png";
                    var img = $('<img>');
                    img.attr('src', "/assets/images/" + imgPath);
                    img.appendTo(spanPos);

                    spanPos.append("&nbps;");
                    spanPos.appendTo(div);

                    var a = $('<a>');
                    a.addClass('thumbnail');
                    var imgFicha = $('<img>');
                    imgFicha.attr('src', '/images/' + result.ficha.id);
                    imgFicha.appendTo(a);
                    a.appendTo(div);

                    var h2 = $('<h2>');
                    var spanNick = $('<span>');
                    spanNick.addClass('label label-info');
                    spanNick.append(result.ficha.nick);
                    spanNick.appendTo(h2);
                    var spanRes = $('<span>');
                    spanRes.addClass("badge pull-right");
                    spanRes.append(result.resultado);
                    spanRes.appendTo(h2);
                    if (isLesionado){
                        var h4 = $('<h4>');
                        h4.addClass('pull-right');
                        var spanLesionText = $('<span>');
                        spanLesionText.addClass("label label-danger");
                        spanLesionText.append(result.lesion.description);
                        spanLesionText.appendTo(h4);
                        h4.append('&nbsp;&nbsp;');
                        h4.appendTo(h2);
                    }
                    h2.appendTo(div);

                    div.appendTo(panel.nativeWidget);
                });
            }
        }

        function addRoutes(){
            self.routes[collectionName] =  crossroads.addRoute('/' + collectionName,  self.handlers.show);
            self.routes.new = crossroads.addRoute('/' + collectionName + '/new', self.handlers.new);
            self.routes.edit = crossroads.addRoute('/' + collectionName + '/{id}', self.handlers.edit);
            self.routes.delete = crossroads.addRoute('/' + collectionName + '/{id}/delete', self.handlers.delete);
            self.routes.addToCollection = crossroads.addRoute('/' + collectionName + '/{id}/' + self.childCollectionName, self.handlers.addToCollection);
            if (self.hasResults === true){
                self.routes.showResult = crossroads.addRoute('/' + collectionName + '/{id}/' + 'clasificacion', self.handlers.showResult);
            }
        }


        self.initialize = initialize;
        self.paintCollection = paintCollection;
        self.buildTableFromCollection = buildTableFromCollection;
        self.paintCollectionItem = paintCollectionItem;
        self.paintCollectionChildWithValue = paintCollectionChildWithValue;
        self.filterCollectionChild = filterCollectionChild;
        self.fillControlOptions = fillControlOptions;
        self.onItemChange = onItemChange;
        self.hasChildCollection = hasChildCollection;
        self.hasValidChildCollectionValues = hasValidChildCollectionValues;
        self.calculateResults = calculateResults;
        self.paintResults = paintResults;

        return self;
    }
    berlin.Interactors.collectionInteractor = collectionInteractor;
})(berlin);