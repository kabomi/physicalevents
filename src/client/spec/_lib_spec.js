var SpecLib = SpecLib || {};

(function(){
    "use strict";
    function setSpyOnIfPossible(obj, method){
        if(obj[method].calls && obj[method].and) return obj[method];
        var spy =  spyOn(obj, method);
        return spy;
    }
    function createTestItems(){
        var testItem = {};

        testItem['grupos'] = {id:0, name:'testName',
                    fichas: [{id:0 , name: 'ficha0'}]}; 
        testItem['fichas'] = {id:0,
                    name:'testName',
                    nick:'testNick',
                    grupo: 'grupo0',
                    caracteristicas: [{id:0 , name: 'caracteristica0', value: 10}]};  
        testItem['eventos'] = {id:0,
                    name:'testName',
                    date:'testDate',
                    grupo: 'grupo0',
                    lesion: '30',
                    caracteristicas: [{id:0 , name: 'caracteristica0', value: 10}]};
        testItem['caracteristicas'] = {id:0,
                    name:'testName',
                    alias:'T'};
        testItem['lesiones'] = {id:0,
                    name:'testName',
                    description:'testDescription'};

        return testItem;
    }
    function createTestCollections(){
        var testCollection = {};

        testCollection['grupos'] = [{id:0 , name: 'ficha0', alias: 'user0'},
                            {id:1 , name: 'ficha1', alias: 'user1'}];
        testCollection['fichas'] = [{id:0 , name: 'caracteristica0', alias: 'car0'},
                            {id:1 , name: 'caracteristica1', alias: 'car1'}];
        testCollection['eventos'] = [{id:0 , name: 'caracteristica0', alias: 'car0'},
                            {id:1 , name: 'caracteristica1', alias: 'car1'}];

        return testCollection;
    }
    function createTestTags(){
        var tags = {};

        tags['grupos'] = ['name', 'fichas'];
        tags['fichas'] = ['name', 'nick', 'grupo', 'caracteristicas'];
        tags['eventos'] = ['name', 'date', 'caracteristicas'];
        tags['caracteristicas'] = ['name', 'alias'];
        tags['lesiones'] = ['name', 'description'];

        return tags;
    }

    SpecLib.setSpyOnIfPossible = setSpyOnIfPossible;
    SpecLib.createTestItems = createTestItems;
    SpecLib.createTestCollections = createTestCollections;
    SpecLib.createTestTags = createTestTags;
})(SpecLib);