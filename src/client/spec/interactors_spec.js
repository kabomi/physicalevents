/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var collectionNames = berlin.Factory.collectionNames;
    var collectionNamesWithChildCollections = berlin.Factory.collectionNamesWithChildCollections;
    describe("Collection Interactors", function(){
        var app, initButtons, panels, collectionItem, testItems;
        var util = berlin.Util;
        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
            initButtons = app.initButtons;
            panels = app.collectionPanels;
            collectionItem = {
                id: 0,
                name: 'test'
            };
            testItems = SpecLib.createTestItems();
            
            spyOn(app.history, 'pushState');
            spyOn(app.history, 'replaceState');
        });

        describe("history", function(){
            it("adds state to history when new button gets clicked", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.show();
                    interactor.newButton.onClick();
                });
                expect(app.history.pushState.calls.count()).toBe(collectionNames.length);
            });
            it("adds state to history when editing collection item", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    var id = name + '-table';
                    interactor.paintInteractor.buildTableFromCollection([collectionItem]);
                    $('#' + id + ' td a:eq(0)').click();
                });
                expect(app.history.pushState.calls.count()).toBe(collectionNames.length);
            });
            it("adds state to history when showing results", function(){
                var name = 'eventos';
                var interactor = app[name + 'Interactor'];
                var id = name + '-table';
                collectionItem.results = [{id:0, name:'test0'}];
                interactor.paintInteractor.buildTableFromCollection([collectionItem]);
                $('#' + id + ' td a:eq(2)').click();
                expect(app.history.pushState).toHaveBeenCalled();
            });
            it("returns to previous state when back from collection/new uris", function(){
                spyOn(history, 'back');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    initButtons[name].onClick();
                    interactor.newButton.onClick();
                    app.backButton.onClick();
                });
                expect(history.back.calls.count()).toBe(collectionNames.length);
            });
            it("replaces state history when editing collection item name", function(){
                spyOn(app.repo, 'updateCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', {updated:true});
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.onItemChange(collectionItem, {name: 'newName'});
                    expect(app.history.replaceState).toHaveBeenCalledWith('/' + name + '/' + 'newName', 'newName'.toUpperCase());
                });
            });
            it("adds state to history when home button click", function(){
                spyOn(app.handlers, 'home').and.callThrough();
                app.homeButton.onClick();
                expect(app.handlers.home).toHaveBeenCalled();
                expect(app.history.pushState).toHaveBeenCalledWith('/');
            });
        });
        describe("panels visibility", function(){
            it("shows edit panel when new button is clicked", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.editPanel, 'show');
                    interactor.handlers.show();
                    interactor.newButton.onClick();
                    expect(interactor.editPanel.show).toHaveBeenCalled();
                });
            });
            it("hides collections panel when new button is clicked", function(){
                spyOn(app.collectionPanels, 'hide');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.show();
                    interactor.newButton.onClick();
                });
                expect(app.collectionPanels.hide.calls.count()).toBeGreaterThan(collectionNames.length);
            });
            it("shows action panel when new button is clicked", function(){
                spyOn(app.actionPanel, 'show');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.show();
                    interactor.newButton.onClick();
                });
                expect(app.actionPanel.show.calls.count()).toBe(collectionNames.length*2);
            });
            it("shows edit panel when editing collection item", function(){
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.editPanel, 'show');
                    interactor.handlers.edit(collectionItem);
                    expect(interactor.editPanel.show).toHaveBeenCalled();
                });
            });
            it("hides collections panel when editing collection item", function(){
                spyOn(app.collectionPanels, 'hide');
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.edit(collectionItem);
                });
                expect(app.collectionPanels.hide.calls.count()).toBe(collectionNames.length);
            });
            it("hides action panel when editing collection item", function(){
                spyOn(app.actionPanel, 'hide');
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.edit(collectionItem);
                });
                expect(app.actionPanel.hide.calls.count()).toBe(collectionNames.length);
            });
            it("hides results panel when editing collection item", function(){
                spyOn(app.resultsPanel, 'hide');
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.edit(collectionItem);
                });
                expect(app.resultsPanel.hide.calls.count()).toBe(collectionNames.length);
            });
            it("hides edit panel when onShow", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.editPanel, 'hide');
                    interactor.handlers.show();
                    expect(interactor.editPanel.hide).toHaveBeenCalled();
                });
            });
            it("hides addToCollection panel when onShow collection", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.addToCollectionPanel, 'hide');
                    interactor.handlers.show();
                    expect(interactor.addToCollectionPanel.hide).toHaveBeenCalled();
                });
            });
            it("hides results panel when onShow collection", function(){
                var interactor = app.eventosInteractor;
                spyOn(app.resultsPanel, 'hide');
                interactor.handlers.show();
                expect(interactor.resultsPanel.hide).toHaveBeenCalled();
            });
            it("hides initButtons when onNew/onEdit", function(){
                spyOn(app.initButtons, 'hide');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.new();
                    interactor.handlers.edit(collectionItem);
                });
                expect(app.initButtons.hide.calls.count()).toBeGreaterThan(collectionNames.length);
            });
            it("shows addToCollection panel when adding items to child collection", function(){
                var interactor;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                    interactor.addToCollectionButton.nativeWidget.click();
                });
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    interactor = app[name + 'Interactor'];
                    spyOn(interactor.addToCollectionPanel, 'show');
                    interactor.handlers.edit(collectionItem);
                    expect(interactor.addToCollectionPanel.show).toHaveBeenCalled();
                });
            });
            it("hides edit panel when adding items to child collection", function(){
                var interactor;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                    interactor.addToCollectionButton.nativeWidget.click();
                });
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    interactor = app[name + 'Interactor'];
                    spyOn(interactor.editPanel, 'hide');
                    interactor.handlers.edit(collectionItem);
                    expect(interactor.editPanel.hide).toHaveBeenCalled();
                });
            });
            it("shows back panel when adding items to child collection", function(){
                var interactor;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', collectionItem);
                    interactor.addToCollectionButton.nativeWidget.click();
                });
                spyOn(app.backPanel, 'show');
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    interactor = app[name + 'Interactor'];
                    interactor.handlers.edit(collectionItem);
                });
                expect(app.backPanel.show.calls.count()).toBe(3);
            });
            it("hides addToCollection panel when onEdit/onNew grupos colleciton item", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.addToCollectionPanel, 'hide');
                    interactor.handlers.edit(collectionItem);
                    expect(interactor.addToCollectionPanel.hide.calls.count()).toBe(1);
                });
            });
            it("shows input file when inserts new item on the server", function(){
                var interactor = app.fichasInteractor;
                spyOn(interactor.repo, 'updateCollectionItem').and.callFake(function(collectionName, item , callback){
                    callback('',{});
                });
                spyOn(util, 'setImageForm').and.callFake(function(itemName, formTag){
                    expect(itemName).not.toBe('testName2');
                    expect(formTag).toBe(interactor.paintInteractor.imageForm);
                });
                interactor.onItemChange(testItems['fichas'], {name: 'testName2'});
                expect(interactor.repo.updateCollectionItem).toHaveBeenCalled();
                expect(util.setImageForm).toHaveBeenCalled();
            });
        });

        describe("get/set data", function(){
            it("get collections from the server", function(){
                var collectionName = 'grupos';
                spyOn(app.repo, 'getCollection').and.callFake(function(name, callback){
                    expect(name).toBe(collectionName);
                });
                initButtons[collectionName].onClick();
            });
            it("has correct route when add child collections using an id instead of an item", function(){
                var collectionName = 'grupos';
                var interactor = app.gruposInteractor;
                var testItem = 'testName';
                var uri = '/' + collectionName + '/' + testItem + '/' + interactor.childCollectionName;
                spyOn(interactor.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', testItem);
                    interactor.addToCollectionButton.onClick(testItem);
                    expect(interactor.history.pushState).toHaveBeenCalledWith(uri, interactor.childCollectionName.toUpperCase());
                });
                interactor.handlers.edit(testItem);
                expect(interactor.repo.getCollectionItem).toHaveBeenCalled();
            });
            it("updates item on the server when onItemChange", function(){
                var interactor = app.fichasInteractor;
                spyOn(interactor.repo, 'updateCollectionItem').and.callFake(function(type, item, callback){
                    callback('', {updated: true});
                    expect(item.name).toBe('testName2');
                });
                interactor.onItemChange(testItems['fichas'], {name: 'testName2'});
                expect(interactor.repo.updateCollectionItem).toHaveBeenCalled();
            });
            it("inserts item on the server when onItemChange", function(){
                var interactor = app.fichasInteractor;
                spyOn(interactor.repo, 'updateCollectionItem').and.callFake(function(type, item, callback){
                    callback('', {updated: true});
                    expect(item.name).toBe('testName2');
                });
                interactor.onItemChange(testItems['fichas'], {name: 'testName2'});
                expect(interactor.repo.updateCollectionItem).toHaveBeenCalled();
            });
            it("doesnt insert item on the server when onItemChange if item has invalid name", function(){
                var interactor = app.fichasInteractor;
                testItems['fichas'].name = undefined;
                spyOn(interactor.repo, 'updateCollectionItem');
                interactor.onItemChange(testItems['fichas'], {nick: 'testName2'});
                expect(interactor.repo.updateCollectionItem).not.toHaveBeenCalled();
            });
            it("only inserts item name when name is not empty", function(){
                var interactor = app.fichasInteractor;
                spyOn(interactor.repo, 'updateCollectionItem');
                interactor.onItemChange(testItems['fichas'], {name: ''});
                expect(interactor.repo.updateCollectionItem).not.toHaveBeenCalled();
            });
            it("only updates item name when name is not 'new'", function(){
                var interactor = app.fichasInteractor;
                spyOn(interactor.repo, 'updateCollectionItem');
                interactor.onItemChange(testItems['fichas'], {name: 'new'});
                expect(interactor.repo.updateCollectionItem).not.toHaveBeenCalled();
            });
            it("only updates item name when name is not taken", function(){
                var collectionName = 'fichas';
                var interactor = app.fichasInteractor;
                var repo = interactor.repo;
                var testData = [{id:0, name:'test0'},
                        {id:1, name:'test1'}];
                repo.collections[collectionName] = testData;
                spyOn(repo, 'updateCollectionItem');
                interactor.onItemChange(testData[0], {name: 'test1'});
                expect(repo.updateCollectionItem).not.toHaveBeenCalled();
            });
            it("only updates item name when name is not empty", function(){
                var collectionName = 'fichas';
                var interactor = app.fichasInteractor;
                var repo = interactor.repo;
                var testData = [{id:0, name:'test0'},
                        {id:1, name:'test1'}];
                repo.collections[collectionName] = testData;
                spyOn(repo, 'updateCollectionItem');
                interactor.onItemChange(testData[0], {name: ''});
                expect(repo.updateCollectionItem).not.toHaveBeenCalled();
            });
        });
    });

    describe("Interactor routes", function(){
        var app;
        var util = berlin.Util;

        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
        });
        it("contains route to collections", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, name, '/' + name);
            });
        });
        it("contains route to edit collection item", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, 'edit', '/' + name + '/0');
            });
        });
        it("contains route to new collection item", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, 'new', '/' + name + '/new');
            });
        });
        it("contains route to delete collection item", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, 'delete', '/' + name + '/0/delete');
            });
        });
        it("contains route to add child collection to collection item", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, 'addToCollection', '/' + name + '/0/' + interactor.childCollectionName);
            });
        });
        it("contains route to add child collection to collection item", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                checkRoute(interactor, 'addToCollection', '/' + name + '/0/' + interactor.childCollectionName);
            });
        });
        it("decodes uri when needed", function(){
            spyOn(app.repo, 'getCollection');
            spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                expect(item).toBe('hello world');
            });
            spyOn(util, 'decodeUri').and.callThrough();
            app.initialize();
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var interactor = app[name + 'Interactor'];
                interactor.handlers.edit('hello%20world');
                interactor.handlers.delete('hello%20world');
                interactor.handlers.addToCollection('hello%20world');
            });
        });
        function checkRoute(interactor, routeName, path){
            expect(interactor.routes[routeName]).toBeDefined();
            expect(interactor.routes[routeName].match(path)).toBeTruthy();
        }

    });

})(SpecLib);