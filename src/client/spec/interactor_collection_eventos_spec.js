/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var collectionName = 'eventos';
    describe("Eventos Collection Interactor", function(){
        var app, initButtons, panels, collectionItem, testItems;
        var util = berlin.Util;
        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
            initButtons = app.initButtons;
            panels = app.collectionPanels;
            collectionItem = {
                id: 0,
                name: 'test'
            };
            testItems = SpecLib.createTestItems();
                

            spyOn(app.history, 'pushState');
            spyOn(app.history, 'replaceState');
        });

        describe("history", function(){
            it("adds state to history when showing results", function(){
                var interactor = app[collectionName + 'Interactor'];
                var id = collectionName + '-table';
                collectionItem.results = [{id:0, name:'test0'}];
                interactor.paintInteractor.buildTableFromCollection([collectionItem]);
                $('#' + id + ' td a:eq(2)').click();
                expect(app.history.pushState).toHaveBeenCalled();
            });
        });
        describe("panels visibility", function(){
            it("shows results panel and hides other panels when onShowResult", function(){
                var interactor = app[collectionName + 'Interactor'];
                testItems[collectionName].results = [{id:0, name:'test0'}];
                spyOn(interactor.collectionPanels, 'hide');
                spyOn(interactor.editPanel, 'hide');
                spyOn(interactor.initButtons, 'hide');
                spyOn(interactor.addToCollectionPanel, 'hide');
                spyOn(interactor.actionPanel, 'hide');
                spyOn(interactor.resultsPanel, 'show');
                spyOn(interactor, 'paintResults');
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', testItems[collectionName]);
                });
                interactor.handlers.showResult(testItems[collectionName]);
                expect(interactor.collectionPanels.hide.calls.count()).toBe(1);
                expect(interactor.editPanel.hide.calls.count()).toBe(1);
                expect(interactor.initButtons.hide.calls.count()).toBe(1);
                expect(interactor.addToCollectionPanel.hide.calls.count()).toBe(1);
                expect(interactor.actionPanel.hide.calls.count()).toBe(1);
                expect(interactor.resultsPanel.show.calls.count()).toBe(1);
            });
            it("doesn't show results panel if a evento isn't launched before and doesn't sum 100% on its collection child values", function(){
                var interactor = app[collectionName + 'Interactor'];
                spyOn(interactor.resultsPanel, 'show');
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                    callback('', testItems[collectionName]);
                });
                spyOn(interactor, 'hasValidChildCollectionValues');
                interactor.handlers.showResult(testItems[collectionName]);
                expect(interactor.hasValidChildCollectionValues).toHaveBeenCalledWith(testItems[collectionName]);
                expect(interactor.resultsPanel.show).not.toHaveBeenCalled();
            });
        });

        describe("get/set data", function(){
            it("gets results if they haven't been calculated", function(){
                var interactor = app[collectionName + 'Interactor'];
                spyOn(app.repo, 'getResults').and.callFake(function(collectionName, id, callback){
                    expect(collectionName).toBe(collectionName);
                    collectionItem.results = [{id:0, name:'test0'}];
                    callback(collectionItem);
                });
                spyOn(interactor, 'paintResults').and.callFake(function(item){
                    expect(item.results).toBeDefined();
                });
                interactor.calculateResults(collectionItem, function(item){
                    expect(item).toBe(collectionItem);
                });
            });
        });
    });

    describe("Interactor routes", function(){
        var app;
        var util = berlin.Util;

        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
        });

        it("contains route to show results", function(){
            var interactor = app[collectionName + 'Interactor'];
            checkRoute(interactor, 'showResult', '/' + collectionName + '/0/' + 'clasificacion');
        });
        it("decodes uri when needed", function(){
            spyOn(app.repo, 'getCollection');
            spyOn(app.repo, 'getCollectionItem').and.callFake(function(collectionName, item, callback){
                expect(item).toBe('hello world');
            });
            spyOn(util, 'decodeUri').and.callThrough();
            app.initialize();
            var interactor = app[collectionName + 'Interactor'];
            interactor.handlers.showResult('hello%20world');
        });
        function checkRoute(interactor, routeName, path){
            expect(interactor.routes[routeName]).toBeDefined();
            expect(interactor.routes[routeName].match(path)).toBeTruthy();
        }

    });

})(SpecLib);