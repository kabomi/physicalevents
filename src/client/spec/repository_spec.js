/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var Factory = berlin.Factory;
    var util = berlin.Util;
    
    describe("repo", function(){
        var serverPath, repo, collectionName, testData;
        beforeEach(function(){
            serverPath = '/data';
            repo = Factory.createRepo(serverPath);
            collectionName = 'grupos';
            testData = [{id:0, name:'test0'},
                        {id:1, name:'test1'}];
        });
        describe("get data", function(){
            it("calls for collection data to a specific server path", function(done){
                function onCollectionFetched(uri, collection){
                    expect(uri).toBe(serverPath + '/' + collectionName);
                    expect(collection[0].id).toBe(0);
                    expect(collection[1].name).toBe('test1');
                    done();
                }
                spyOn(repo, 'getData').and.callFake(function(uri, callback){
                    callback(uri, testData);
                });
                repo.getCollection(collectionName, onCollectionFetched);
            });

            it("calls for collection item data to a specific server path using an item", function(done){
                function onCollectionItemFetched(uri, item){
                    expect(uri).toBe(serverPath + '/' + collectionName + '/' + testData[0].id);
                    expect(item.id).toBe(0);
                    expect(item.name).toBe('test0');
                    done();
                }
                spyOn(repo, 'getData').and.callFake(function(uri, callback){
                    callback(uri, testData[0]);
                });
                repo.getCollectionItem(collectionName, testData[0], onCollectionItemFetched);
            });
            it("calls for collection item data to a specific server path using an item id value", function(done){
                function onCollectionItemFetched(uri, item){
                    expect(uri).toBe(serverPath + '/' + collectionName + '/' + testData[0].id);
                    done();
                }
                spyOn(repo, 'getData').and.callFake(function(uri, callback){
                    callback(uri, testData[0]);
                });
                repo.getCollectionItem(collectionName, testData[0].id, onCollectionItemFetched);
            });
            it("calls for collection item data to a specific server path using an item name value", function(done){
                function onCollectionItemFetched(uri, item){
                    expect(uri).toBe(serverPath + '/' + collectionName + '/' + testData[0].name);
                    done();
                }
                spyOn(repo, 'getData').and.callFake(function(uri, callback){
                    callback(uri, testData[0]);
                });
                repo.getCollectionItem(collectionName, testData[0].name, onCollectionItemFetched);
            });
            it("stores collection data when fetched from the server", function(done){
                function onCollectionFetched(uri, collection){
                    expect(repo.collections[collectionName][0].id).toBe(0);
                    expect(repo.collections[collectionName][1].name).toBe('test1');
                    done();
                }
                spyOn(util, 'ajaxGet').and.callFake(function(uri, success, error){
                    success(testData);
                });
                repo.getCollection(collectionName, onCollectionFetched);
            });
            it("stores collection item data when fetched from the server", function(done){
                var newData = {id:0, name:'test5000'};
                repo.collections[collectionName] = testData;
                function onCollectionItemFetched(uri, item){
                    expect(repo.collections[collectionName][1].id).toBe(0);
                    expect(repo.collections[collectionName][1].name).toBe('test5000');
                    done();
                }
                spyOn(util, 'ajaxGet').and.callFake(function(uri, success, error){
                    success(newData);
                });
                repo.getCollectionItem(collectionName, testData[0], onCollectionItemFetched);
            });
            it("calls for results data to a specific server path using an item id", function(done){
                var results = [{id:0, nick: 'nick0', position: 1, resultado: ''}];
                function onCollectionItemFetched(uri, item){
                    expect(uri).toBe(serverPath + '/' + collectionName + '/' + testData[0].id + '/' + 'clasificacion');
                    expect(item.id).toBe(0);
                    expect(item.name).toBe('test0');
                    expect(item.results).toBe(results);
                    done();
                }
                spyOn(repo, 'getData').and.callFake(function(uri, callback){
                    testData[0].results = results;
                    callback(uri, testData[0]);
                });
                repo.getResults(collectionName, testData[0].id, onCollectionItemFetched);
            });
        });
        describe("set data", function(){
            it("deletes specific collection item", function(done){
                var idToDelete = testData[0].id;
                repo.collections[collectionName] = testData;
                spyOn(repo, 'deleteData').and.callFake(function(uri, data, callback){
                    expect(testData[0].id).toBe(1);
                    expect(data.id).toBe(idToDelete);
                    callback(uri, {deleted:true});
                });
                function onResponse(uri, response){
                    expect(response.deleted).toBeTruthy();
                    done();
                }
                repo.deleteCollectionItem(collectionName, testData[0], onResponse);
            });
            it("removes item from collection data when requested to the server", function(done){
                repo.collections[collectionName] = testData;
                function onResponse(uri, response){
                    expect(repo.collections[collectionName].length).toBe(1);
                    expect(repo.collections[collectionName][0].id).toBe(1);
                    expect(repo.collections[collectionName][0].name).toBe('test1');
                    done();
                }
                spyOn(util, 'ajaxDelete').and.callFake(function(uri, data, success, error){
                    success(testData);
                });
                repo.deleteCollectionItem(collectionName, testData[0], onResponse);
            });
            it("updates specific collection item", function(done){
                repo.collections[collectionName] = testData;
                spyOn(repo, 'putData').and.callFake(function(uri, data, callback){
                    expect(data.id).toBe(0);
                    expect(data.name).toBe('testUpdate');
                    callback(uri, {updated:true});
                });
                function onResponse(uri, response){
                    expect(response.updated).toBeTruthy();
                    expect(testData[0].name).toBe('testUpdate');
                    expect(repo.collections[collectionName].length).toBe(2);
                    done();
                }
                repo.updateCollectionItem(collectionName, {id: 0, name: 'testUpdate'}, onResponse);
            });
            it("updates item from collection data when requested to the server", function(done){
                repo.collections[collectionName] = testData;
                function onResponse(uri, response){
                    expect(repo.collections[collectionName].length).toBe(2);
                    expect(repo.collections[collectionName][0].id).toBe(0);
                    expect(repo.collections[collectionName][0].name).toBe('testUpdate');
                    done();
                }
                spyOn(util, 'ajaxPut').and.callFake(function(uri, data, success, error){
                    success(testData);
                });
                repo.updateCollectionItem(collectionName, {id:0, name: 'testUpdate'}, onResponse);
            });
        });
    });
})();