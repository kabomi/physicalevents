/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var Factory = berlin.Factory;
    var util = berlin.Util;
    var collectionNames = Factory.collectionNames;
    var tags = {};

    describe("Login", function(){
        it("should request user info", function (done){
            var loginBox = Factory.createLogin('login-box');
            loginBox.setUser('test');
            loginBox.setPass('test');

            var foo = {
                callback : function(response){
                    expect(true).toBeTruthy();
                    done();
                },
                errorCallback: function(error){
                    expect(true).toBeTruthy();
                    done();
                }
            };
            var loginButton = $('#login-box-button');
            loginButton.on('click', function(){ loginBox.logIn(foo.callback, foo.errorCallback);});
            loginButton.click();
        });
    });

    describe("Initial actions", function(){
        it("should show buttons", function(){
            var initButtonBox = Factory.createInitButtons(collectionNames);
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                spyOn(initButtonBox[name], 'show');
                initButtonBox.show();
                expect(initButtonBox[name].show).toHaveBeenCalled();
            });
        });
    });
    describe("Collections", function(){
        var app, interactor, editTags, collectionName, testItems;
        beforeEach(function(){
            app = {
                repo: {
                    getCollection: function(){}
                }
            };
            tags = SpecLib.createTestTags();
            testItems = SpecLib.createTestItems();
            
        });
        it("adds edit tags to collection interators", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                util.applyFunctionToArguments(checkEditTags, util.capitalizeFirstLetter(name),  tags[name]);
            });
        });
        function checkEditTags(name, fieldNames){
            interactor = Factory['create' + name + 'Interactor'](app);
            editTags = interactor.editTags;

            expect(editTags).toBeDefined();
            fieldNames.forEach(function(fieldName){
                expect(editTags[fieldName]).toBeDefined();
            });
        }
        it("it builds a collection table", function(){
            collectionName = 'fichas';
            var id = collectionName + '-table';
            var array = [{"_id":"531afad3d0cd9c509dd555ab","id":1,"name":"ficha1","nick":"menganito","grupo":"bachiller1","caracteristicas":[{"name":"car1","value":"5"},{"name":"car2","value":"7"}]},
            {"_id":"531afad3d0cd9c509dd555ac","id":2,"name":"ficha2","nick":"pepito","grupo":"eso1","caracteristicas":[{"name":"car0","value":"10"}]},
            {"_id":"531b2f0ad0cd9c509dd555b3","id":3,"name":"ficha3"},{"_id":"531afad3d0cd9c509dd555aa","id":0,"name":"ficha0","nick":"fulanito","caracteristicas":[{"name":"car2","value":"3"}]}
            ];
            Factory.createTable(collectionName);
            util.buildCollectionTable(collectionName, array, {
                TEST: function(item){
                    expect(item).toBe(array[0]);
                },
                TEST2: function(item){
                    expect(item).toBe(array[0]);
                }
            });
            expect($('#' + id).length).toBe(1);
            expect($('#' + id + ' tr').length).toBe(5);
            expect($('#' + id + ' td').length).toBeGreaterThan(0);
            var tr1FirstAction = $('#' + id + ' tr:eq(1) td:eq(-2) a');
            var tr1SecondAction = $('#' + id + ' tr:eq(1) td:eq(-1) a');
            expect(tr1FirstAction.text()).toBe('TEST');
            expect(tr1SecondAction.text()).toBe('TEST2');
            tr1FirstAction.click();
            tr1SecondAction.click();
        });
        it("sets a field from tag with specific value", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                var createMethod = 'create' + util.capitalizeFirstLetter(name) + 'Interactor';
                interactor = Factory[createMethod](app);
                interactor.paintInteractor.paintCollectionItem(testItems[name]);
                checkPaintCollectionItem(interactor, testItems[name]);
            });
        });
        function checkPaintCollectionItem(interactor, testItem){
            var editTags = interactor.editTags;
            for(var name in editTags){
                if(editTags.hasOwnProperty(name)){
                    var tag = editTags[name];
                    if(editTags.hasOwnProperty(name)){
                        var itemFieldValue = testItem[name];
                        var obj = $('#' + tag.id + '-' + tag.type);
                        expect(obj.length).toBe(1);
                        expect(obj[0].tagName).toBe(tag.type.toUpperCase());
                        var li = obj.find('li');
                        if(li.length > 0){
                            //console.log("TAG:" + JSON.stringify(tag));
                            var expected = (li.text() === itemFieldValue[0].name) ||
                                    (li.text() === itemFieldValue[0].name + ': ' + itemFieldValue[0].value);
                            expect(expected).toBeTruthy();
                            var input = li.find('input[data-validation]');
                            var hasInput = (tag.noInput === undefined);
                            if (hasInput){
                                expect(input.length).toBe(1);
                                expect(input.attr('data-validation')).toBe('number');
                                expect(input.attr('data-validation-allowing')).toBe("range[0;100]");
                            }else{
                                expect(input.length).toBe(0);
                            }
                        }else{
                            expect(obj.val()).toBe(itemFieldValue);
                        }
                        if(tag.type === 'select' && itemFieldValue){
                            expect(obj[0].options.length).toBeGreaterThan(1);
                        }
                    }
                }
            }
        }
    });

    describe("Util", function(){
        it("empties table before when building a collection table", function(){
            var name = 'test';
            var testObj = {
                empty: function(){},
                nativeWidget: {}
            };
            spyOn(Factory, 'createTable').and.callFake(function(collectionName){
                expect(collectionName).toBe(name);
                return testObj;
            });
            spyOn(testObj, 'empty');
            util.buildCollectionTable(name);
            expect(Factory.createTable).toHaveBeenCalled();
            expect(testObj.empty).toHaveBeenCalled();
        });
        it("empties list when setting field from tag", function(){
            var name = 'test';
            var testObj = {
                empty: function(){}
            };
            var testTag = {
                id: 'test',
                type: 'ul'
            };
            spyOn(Factory, 'createList').and.callFake(function(collectionName){
                expect(collectionName).toBe(name);
                return testObj;
            });
            spyOn(testObj, 'empty');
            util.setFieldFromTagWithValue(testTag);
            expect(Factory.createList).toHaveBeenCalled();
            expect(testObj.empty).toHaveBeenCalled();
        });
        it("empties select when setting field from tag", function(){
            var name = 'test';
            var testObj = {
                empty: function(){},
                unbind: function(){},
                bind: function(){}
            };
            var testTag = {
                id: 'test',
                type: 'select'
            };
            spyOn(Factory, 'createSelect').and.callFake(function(collectionName){
                expect(collectionName).toBe(name);
                return testObj;
            });
            spyOn(testObj, 'empty');
            util.setFieldFromTagWithValue(testTag);
            expect(Factory.createSelect).toHaveBeenCalled();
            expect(testObj.empty).toHaveBeenCalled();
        });
        it("hides button when setClassificationButton when item has no classification field", function(){
            var collectionName = 'eventos';
            var testItem = {};
            var id = collectionName + '-classification-button';
            util.setClassificationButton(collectionName, testItem);
            var object = $('#' + id + ':visible');
            expect(object.length).toBe(0);
        });
    });
})(SpecLib);