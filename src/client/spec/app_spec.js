/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var util = berlin.Util;

    describe("karma", function(){
        
        it("runs tests properly", function () {
            expect(true).toBeTruthy();
        });
    });

    describe("Initialization", function(){
        var app;
        beforeEach(function(){
            app = new berlin.App();
        });

        it("should show init buttons", function(){
            util.printObj(app, "APP");
            expect(app.initButtons).toBeDefined();
            
            spyOn(app.initButtons, 'show');
            app.initialize();

            expect(app.initButtons.show).toHaveBeenCalled();
        });
        it("should hide back panel", function(){
            var backPanel = app.backPanel;
            expect(backPanel).toBeDefined();
            
            spyOn(backPanel, 'hide');
            app.initialize();

            expect(backPanel.hide).toHaveBeenCalled();
        });
        it("should hide collection panels", function(){
            var panels = app.collectionPanels;
            spyOn(panels, 'hide');
            app.initialize();

            expect(panels.hide).toHaveBeenCalled();
        });
        it("should hide action panel", function(){
            var panel = app.actionPanel;
            spyOn(panel, 'hide');
            app.initialize();

            expect(panel.hide).toHaveBeenCalled();
        });
        it("should hide results panel", function(){
            var panel = app.resultsPanel;
            spyOn(panel, 'hide');
            app.initialize();

            expect(panel.hide).toHaveBeenCalled();
        });
        it("contains route to home", function(){
            var name = "home";
            var uri = "/";
            app.initialize();
            expect(app.routes[name]).toBeDefined();
            expect(app.routes[name].match(uri)).toBeTruthy();
        });
    });

    describe("Collections", function(){
        var app, initButtons, panels;
        var collectionNames = ['grupos', 'fichas', 'eventos'];
        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
            initButtons = app.initButtons;
            panels = app.collectionPanels;
            spyOn(app.history, 'pushState');
        });
        
        it("shows specific collection panel when initial button gets clicked", function(done){
            util.applyFunctionToArrayItems(collectionNames, checkShowsCollectionWhenButtonClicked);
            done();
        });
        function checkShowsCollectionWhenButtonClicked(type){
            SpecLib.setSpyOnIfPossible(panels, 'hide');
            spyOn(panels[type], 'show');
            $('#' + type + '-button').click();

            expect(panels[type].show).toHaveBeenCalled();
            expect(panels.hide).toHaveBeenCalled();

            spyOn(initButtons[type], 'onClick');
            $('#' + type + '-button').click();

            expect(initButtons[type].onClick).toHaveBeenCalled();
        }
        it("hides initial actions when initial button gets clicked", function(done){
            util.applyFunctionToArrayItems(collectionNames, checkHideInitialButtonsWhenButtonClicked);
            done();
        });
        function checkHideInitialButtonsWhenButtonClicked(type){
            SpecLib.setSpyOnIfPossible(initButtons, 'hide');
            $('#' + type + '-button').click();

            expect(initButtons.hide).toHaveBeenCalled();
        }
        it("shows back button when initial button gets clicked", function(){
            spyOn(app.backPanel, 'show');
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                initButtons[name].onClick();
            });
            expect(app.backPanel.show.calls.count()).toBe(collectionNames.length);
        });

        it("shows new action button when initial button gets clicked", function(){
            var collectionName = 'grupos';
            spyOn(app.actionPanel, 'show');
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                initButtons[name].onClick();
            });
            expect(app.actionPanel.show.calls.count()).toBe(collectionNames.length);
        });
        it("adds state to history when initial button gets clicked", function(){
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                initButtons[name].onClick();
                expect(app.history.pushState).toHaveBeenCalledWith('/' + name, name.toUpperCase());
            });
        });
        it("removes state from history when back button gets clicked", function(){
            var backButton = app.backButton;
            spyOn(app.history, 'popState');
            backButton.onClick();

            expect(app.history.popState).toHaveBeenCalled();
        });
        it("returns to previous state when back from collection uris", function(){
            initButtons.grupos.onClick();
            var backButton = app.backButton;
            spyOn(history, 'back');
            backButton.onClick();

            expect(history.back).toHaveBeenCalled();
        });
        it("get collections from the server", function(){
            var collectionName;
            spyOn(app.repo, 'getCollection').and.callFake(function(name, callback){
                expect(name).toBe(collectionName);
            });
            util.runCallbackForEachArrayItem(collectionNames, function(name){
                collectionName = name;
                initButtons[name].onClick();
            });
        });
    });

    describe("Crossroads", function(){
        var app;
        beforeEach(function(){
            app = new berlin.App();
            spyOn(app.history, 'pushState');
            spyOn(app.history, 'popState');
        });

        it("ignores state on initialization", function(){
            app.initialize();
            expect(crossroads.ignoreState).toBeTruthy();
        });
        it("not parses state on initialization if not necessary", function(){
            spyOn(crossroads, "parse").and.callThrough();
            spyOn(util, 'isNotFiringOnPopStateEventOnLoad').and.returnValue(false);
            app.initialize();
            expect(crossroads.parse).not.toHaveBeenCalled();
        });
        it("parses state on window.onpopstate", function(){
            spyOn(crossroads, "parse").and.callThrough();
            window.onpopstate({preventDefault: function(){
                expect(true).toBeTruthy();
            }});
            expect(crossroads.parse).toHaveBeenCalled();
        });
    });
})(SpecLib);