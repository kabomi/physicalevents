/*global describe, it, expect, spyOn, beforeEach, fs, xit, done, jasmine, afterEach, xdescribe */

(function(){
    "use strict";

    var collectionNames = berlin.Factory.collectionNames;
    var collectionNamesWithChildCollections = berlin.Factory.collectionNamesWithChildCollections;
    describe("Paint Interactor", function(){
        var app, initButtons, panels, collectionItem, testItems, testCollections;
        var util = berlin.Util;
        beforeEach(function(){
            app = new berlin.App();
            app.initialize();
            initButtons = app.initButtons;
            panels = app.collectionPanels;
            collectionItem = {
                id: 0,
                name: 'test'
            };
            testItems = SpecLib.createTestItems();
            testCollections = SpecLib.createTestCollections();
            
            spyOn(app.history, 'pushState');
            spyOn(app.history, 'replaceState');
        });

        describe("paint controls", function(){
            it("knows how to paint collections", function(){
                var collection = [collectionItem];
                spyOn(app.repo, 'getData').and.callFake(function(uri, callback){
                    callback(uri, collection);
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor.paintInteractor, 'paintCollection').and.callThrough();
                    spyOn(interactor.paintInteractor, 'buildTableFromCollection');
                    initButtons[name].onClick();
                    expect(interactor.paintInteractor.buildTableFromCollection).toHaveBeenCalled();
                    expect(interactor.paintInteractor.buildTableFromCollection.calls.first().args[0]).toBe(collection);
                    expect(interactor.paintInteractor.paintCollection).toHaveBeenCalled();
                });
            });
            it("paints collection table with actions", function(){
                var collectionName;
                var interactor;
                spyOn(util, 'buildCollectionTable').and.callFake(function(name, array, actions){
                    expect(name).toBe(collectionName);
                    expect(actions.EDITAR).toBeDefined();
                    expect(actions.BORRAR).toBeDefined();
                    if(name === 'eventos'){
                        expect(actions.LANZAR).toBeDefined();
                    }else{
                        expect(actions.LANZAR).toBeUndefined();
                    }
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    interactor = app[name + 'Interactor'];
                    var collection = [collectionItem];
                    collectionName = name;
                    interactor.paintInteractor.buildTableFromCollection(collection);
                });
                expect(util.buildCollectionTable.calls.count()).toBe(collectionNames.length);
            });
            it("creates new action button when onShow", function(){
                spyOn(berlin.Factory, 'createNewButton').and.callFake(function(){
                    return {onClick: function(){}};
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.handlers.show();
                });
                expect(berlin.Factory.createNewButton.calls.count()).toBe(collectionNames.length);
            });
            it("deletes a collection item when onDelete", function(){
                spyOn(util, 'prompt');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    var testId = 0;
                    interactor.handlers.delete(testId);
                });
                expect(util.prompt.calls.count()).toBe(collectionNames.length);
            });
            it("edits the correct item when onEditClick", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    var id = name + '-table';
                    spyOn(interactor.handlers, 'edit');
                    interactor.paintInteractor.buildTableFromCollection([collectionItem]);
                    $('#' + id + ' td a:eq(0)').click();
                    expect(interactor.handlers.edit).toHaveBeenCalledWith(collectionItem);
                });
            });
            it("paints results when onShowResultClick", function(){
                var name = 'eventos';
                var interactor = app[name + 'Interactor'];
                var id = name + '-table';
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', item);
                });
                spyOn(interactor.handlers, 'showResult').and.callThrough();
                spyOn(interactor, 'calculateResults').and.callThrough();
                spyOn(interactor, 'paintResults').and.callFake(function(item){
                    var isArray = util.isArray(item.results);
                    expect(isArray).toBeTruthy();
                    expect(item.grupo).toBeDefined();
                    expect(item.lesion).toBeDefined();
                    expect(item.lesion).toBeGreaterThan(-1);
                });
                collectionItem.lesion = 30;
                collectionItem.grupo = 'grupo0';
                collectionItem.results = [{id:0, name:'test0'}];
                interactor.paintInteractor.buildTableFromCollection([collectionItem]);
                $('#' + id + ' td a:eq(2)').click();
                expect(interactor.calculateResults).toHaveBeenCalled();
                expect(interactor.handlers.showResult).toHaveBeenCalledWith(collectionItem);
                expect(interactor.paintResults).toHaveBeenCalled();
            });
            it("updates item when onchange", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.onItemChange(testItems[name], {nick: 'testNick1'});
                    expect(testItems[name].nick).toBe('testNick1');
                });
            });
            it("does not edit item when onchange if trying to change id or _id", function(){
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    var item = testItems[name];
                    interactor.onItemChange(item, {id: 1});
                    expect(item.id).toBe(0);
                    item._id = 0;
                    interactor.onItemChange(item, {_id: 1});
                    expect(item._id).toBe(0);
                });
            });
            it("trims value before onchange its fired", function(){
                var collectionName;
                spyOn(util, 'setFieldFromTagWithValue').and.callFake(function(tag, itemFieldValue, callback){
                   callback({name: '      trimmed    '});
                });
                spyOn(app.repo, 'updateCollectionItem').and.callFake(function(collectionName, item){
                    expect(item.name).toBe('trimmed');
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    collectionName = name;
                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                });
                expect(app.repo.updateCollectionItem.calls.count()).toBeGreaterThan(collectionNames.length);
            });
            it("paints a collection item when onEdit", function(){
                var collectionName;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', testItems[collectionName]);
                });
                spyOn(util, 'setFieldFromTagWithValue').and.callFake(function(tag, itemFieldValue){
                    expect(tag.test).toBe('name0');
                    expect(itemFieldValue).toBe('testName');
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    collectionName = name;
                    interactor.editTags = {
                        name: {id:0, test:'name0'}
                    };
                    spyOn(interactor.paintInteractor, 'paintCollectionItem').and.callThrough();
                    interactor.handlers.edit(testItems[name]);
                    expect(interactor.paintInteractor.paintCollectionItem).toHaveBeenCalled();
                });
                expect(util.setFieldFromTagWithValue.calls.count()).toBe(collectionNames.length);
            });
            it("paints form and image control when onEdit fichas collection item", function(){
                var collectionName = 'fichas';
                var interactor = app[collectionName + 'Interactor'];
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', testItems[collectionName]);
                });
                spyOn(util, 'setImageForm').and.callFake(function(itemId, imageForm){
                    expect(itemId).toBe(testItems[collectionName].id);
                    expect(imageForm).toBe(interactor.paintInteractor.imageForm);
                });
                interactor.editTags = {
                    name: {id:0, test:'name0'}
                };
                spyOn(interactor.paintInteractor, 'paintCollectionItem').and.callThrough();
                interactor.handlers.edit(testItems[collectionName]);
                expect(interactor.paintInteractor.paintCollectionItem).toHaveBeenCalled();
                expect(util.setImageForm).toHaveBeenCalled();
            });
            it("paints button control when onEdit evento with a results collection", function(){
                var collectionName = 'eventos';
                var interactor = app[collectionName + 'Interactor'];
                testItems[collectionName].results = [{id:0, name:'ficha0'}];
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', testItems[collectionName]);
                });
                spyOn(util, 'setClassificationButton');
                interactor.editTags = {
                    name: {id:0, test:'name0'}
                };
                spyOn(interactor.paintInteractor, 'paintCollectionItem').and.callThrough();
                interactor.handlers.edit(testItems[collectionName]);
                expect(interactor.paintInteractor.paintCollectionItem).toHaveBeenCalled();
                expect(util.setClassificationButton).toHaveBeenCalled();
            });
            it("paints a collection item when onNew", function(){
                var interactor;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', {});
                    expect(interactor.paintInteractor.paintCollectionItem).toHaveBeenCalled();
                });
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    interactor = app[name + 'Interactor'];
                    spyOn(interactor.paintInteractor, 'paintCollectionItem').and.callThrough();
                    interactor.handlers.new();
                });
            });
            it("allows to add child collections", function(){
                var collectionName;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(type, item, callback){
                    callback('', testItems[collectionName]);
                });
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var uri = '/' + name + '/' + testItems[name].name + '/' + interactor.childCollectionName;
                    collectionName = name;
                    interactor.handlers.edit(testItems[name]);
                    expect(interactor.addToCollectionButton).toBeDefined();
                    $('#' + name + '-addToCollection' + '-button').click();
                    expect(interactor.history.pushState).toHaveBeenCalledWith(uri, interactor.childCollectionName.toUpperCase());
                });
            });
            it("paints collection child", function(){
                var interactor, collectionName;
                spyOn(app.repo, 'getCollectionItem').and.callFake(function(name, item, callback){
                    callback('', testItems[collectionName]);
                });
                spyOn(app.repo, 'getCollection').and.callFake(function(name, callback){
                    callback('', testCollections[collectionName]);
                });
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    interactor = app[name + 'Interactor'];
                    collectionName = name;
                    spyOn(interactor.paintInteractor, 'paintCollectionChildWithValue');
                    interactor.handlers.addToCollection(testItems[name]);
                    expect(interactor.paintInteractor.paintCollectionChildWithValue).toHaveBeenCalledWith(testCollections[name], testItems[name]);
                });
            });
            it("paints collection child with controls", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    var obj = $('#' + name + '-' + interactor.childCollectionName + '-panel');
                    console.log("DIV HTML: " + obj.html());
                    expect(obj.find("input").length).toBe(2);
                    expect(obj.find("span").length).toBe(4);
                    expect(obj.find(":checkbox:eq(0)").is(':checked')).toBe(true);
                    expect(obj.find(":checkbox:eq(1)").is(':checked')).toBe(false);
                });
            });
            it("paints results with controls", function(){
                var collectionName = "eventos";
                var interactor = app[collectionName + 'Interactor'];
                collectionItem.results = [{position:1, ficha: {
                    id:1,
                    name:'test1',
                    nick: 'nick1'
                }, resultado: 'resultado1'}];
                interactor.paintResults(collectionItem);
                var firstFicha = collectionItem.results[0];
                var obj = $('#results-body' + '-panel');
                console.log("RESULTS DIV HTML: " + obj.html());
                expect(obj.find("span").length).toBe(3);
                expect(obj.find("span:eq(0) img:eq(0)").attr('src')).toContain("/images/" + "Trophy-gold.png");
                expect(obj.find("img:eq(1)").attr('src')).toContain('/images/' + firstFicha.ficha.id);
                expect(obj.find("span:eq(1)").html()).toBe(firstFicha.ficha.nick);
                expect(obj.find("span:eq(2)").html()).toBe(firstFicha.resultado);
            });
        });
        describe("item controls", function(){
            it("has an onchange event for input name controls", function(){
                spyOn(util, 'setImageForm');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app[name + 'Interactor'];
                    spyOn(interactor, 'onItemChange');
                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                    var obj = $('#' + name + '-name-input');
                    obj.val('test1');
                    obj.change();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                    expect(interactor.onItemChange.calls.first().args[0]).toBe(testItems[name]);
                    expect(interactor.onItemChange.calls.first().args[1].name).toBe('test1');
                });
            });
            it("handles a change event for image input control", function(){
                var collectionName = 'fichas';
                var interactor = app.fichasInteractor;
                spyOn(util, 'ajaxPostFile').and.callFake(function(uri, data){
                    expect(uri).toBe('/image');
                    expect(data).toBeDefined();
                });
                spyOn(util, 'setFieldFromTagWithValue');
                interactor.paintInteractor.paintCollectionItem(testItems[collectionName]);
                var obj = $('#' + collectionName + '-form input:file');
                obj.change();
                expect(util.ajaxPostFile).toHaveBeenCalled();
            });
            it("has an onchange event for input child collection controls", function(){
                spyOn(util, 'isInputValid').and.returnValue(true);
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    if (name === 'grupos') return;
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        expect(objectValue[childCollectionName][0].value).toBe('test1');
                    });
                    var obj = $('#' + name + '-' + childCollectionName + '-ul input:eq(0)');
                    obj.val('test1');
                    obj.change();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("has an onchange event for select controls", function(){
                var interactor = app.fichasInteractor;
                var testItem = {id:0,
                    name:'testName',
                    nick:'testNick',
                    grupo: 'grupo0',
                    caracteristicas: [{id:0 , name: 'caracteristica0', value: 10}]};
                interactor.paintInteractor.paintCollectionItem(testItem);
                spyOn(interactor, 'onItemChange');
                var obj = $('#fichas-grupo-select');
                var option = $('<option>').append('test1');
                option.appendTo(obj);
                obj.val('test1');
                obj.change();
                expect(interactor.onItemChange).toHaveBeenCalled();
                expect(interactor.onItemChange.calls.first().args[0]).toBe(testItem);
                expect(interactor.onItemChange.calls.first().args[1].grupo).toBe('test1');
            });
            it("has a combo for select controls", function(){
                var interactor = app.fichasInteractor;
                interactor.childCollectionUlSelect = undefined;
                var testItem = {id:0, name:'test0'};
                spyOn(interactor.paintInteractor, 'fillControlOptions').and.callThrough();
                spyOn(interactor.repo, 'getCollection').and.callFake(function(collectionName, callback){
                    expect(collectionName).toBe('grupos');
                    callback('', [{name: 'testGrupo0'}]);
                });
                interactor.paintInteractor.paintCollectionItem(testItem);
                var obj = $('#fichas-grupo-select');
                expect(interactor.paintInteractor.fillControlOptions).toHaveBeenCalled();
                expect(obj.find("option:eq(1)").val()).toBe('testGrupo0');
            });
            it("validates data before firing input onchange event", function(){
                spyOn(util, 'isInputValid');
                util.runCallbackForEachArrayItem(collectionNames, function(name){
                    var interactor = app.fichasInteractor;
                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                    var obj = $('#' + name + '-name-input');
                    obj.val('test1');
                    obj.change();
                    expect(util.isInputValid.calls.first().args[0].val()).toBe('test1');
                });
            });
            it("fills filter options when needed", function(){
                spyOn(util, 'fillCollectionChildSelect').and.callThrough();
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var spy = SpecLib.setSpyOnIfPossible(interactor.repo, 'getCollection');
                    spy.and.callFake(function(collectionName, callback){
                        if (collectionName === 'grupos'){
                            callback('', [{name: 'testGrupo'}]);
                            return;
                        }
                        callback('', [{name: 'testOption'}]);
                        if (interactor.childCollectionUlSelect !== undefined){
                            expect(util.fillCollectionChildSelect).toHaveBeenCalled();
                            var select = $('#' + name + '-' + childCollectionName + '-ul-filter' + '-select');
                            expect(select.val()).toBe('Todos');
                            expect(select.children().length).toBe(2);
                            expect(select.children()[1].text).toBe('testOption');
                        }
                    });
                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                });
            });
            it("filters collectionChild items when on filter", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var validItemName =  'caracteristica0';
                    var spy = SpecLib.setSpyOnIfPossible(interactor.repo, 'getCollection');
                    var panel = berlin.Factory.createPanel(name + '-' + childCollectionName + '-ul').nativeWidget;
                    var ul = berlin.Factory.createList(name + '-' + childCollectionName).nativeWidget;
                    ul.appendTo(panel);
                    spy.and.callFake(function(collectionName, callback){
                        if (collectionName === 'grupos'){
                            callback('', [{name: 'testGrupo'}]);
                            return;
                        }
                        callback('', [{name: 'testOption'}]);
                    });
                    SpecLib.setSpyOnIfPossible(interactor.repo, 'getCollectionItem').and.callFake(function(collectionName, itemName, callback){
                        callback('', [{name: validItemName}]);
                    });
                    spyOn(interactor.paintInteractor, 'filterCollectionChild').and.callFake(function(itemName, callback){
                        callback([]);
                        var firstLi = panel.find('li:eq(0):visible');
                        expect(firstLi.length).toBe(0);
                        callback([{name: validItemName}]);
                        firstLi = panel.find('li:eq(0):visible');
                        expect(firstLi.length).toBe(1);
                    });

                    interactor.paintInteractor.paintCollectionItem(testItems[name]);
                    var select = $('#' + name + '-' + childCollectionName + '-ul-filter' + '-select');
                    select.val('testOption');
                    select.change();
                });
            });
        });
        describe("child collection controls", function(){
            it("has an onchange event for input controls", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        expect(item).toBe(testItems[name]);
                        var value = objectValue[childCollectionName][1];
                        expect(value.id).toBe(testCollections[name][1].id);
                        expect(value.name).toBe(testCollections[name][1].name);
                    });
                    var obj = $('#' + name + '-' + childCollectionName + '-panel');
                    var checkInput = obj.find(':checkbox:eq(1)');
                    checkInput.prop('checked', true);
                    checkInput.change();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("selects all the checkboxes when selectAll button is clicked", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var panel = $('#' + name + '-' + childCollectionName + '-panel');
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        expect(item).toBe(testItems[name]);
                        var value = objectValue[childCollectionName][1];
                        expect(value.id).toBe(testCollections[name][1].id);
                        expect(value.name).toBe(testCollections[name][1].name);
                        var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                        expect(firstCheckbox.prop('checked')).toBe(true);
                    });
                    var button = $('#' + name + '-' + childCollectionName + '-selectAll-button');
                    var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                    firstCheckbox.prop('checked', false);
                    button.click();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("deselects all the checkboxes when deselectAll button is clicked", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var panel = $('#' + name + '-' + childCollectionName + '-panel');
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        var value = objectValue[childCollectionName][1];
                        expect(value).toBeUndefined();
                        var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                        expect(firstCheckbox.prop('checked')).toBe(false);
                    });
                    var button = $('#' + name + '-' + childCollectionName + '-deselectAll-button');
                    var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                    firstCheckbox.prop('checked', true);
                    button.click();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("selects only visible checkboxes", function (){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    var panel = $('#' + name + '-' + childCollectionName + '-panel');
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                        expect(firstCheckbox.prop('checked')).toBe(false);
                    });
                    var button = $('#' + name + '-' + childCollectionName + '-selectAll-button');
                    panel.find('div').hide();
                    var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                    firstCheckbox.prop('checked', false);
                    expect(firstCheckbox.prop('checked')).toBe(false);
                    button.click();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("deselects only visible checkboxes", function (){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    var panel = $('#' + name + '-' + childCollectionName + '-panel');
                    spyOn(interactor, 'onItemChange').and.callFake(function(item, objectValue){
                        var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                        expect(firstCheckbox.prop('checked')).toBe(true);
                    });
                    var button = $('#' + name + '-' + childCollectionName + '-deselectAll-button');
                    panel.find('div').hide();
                    var firstCheckbox = panel.find('input[type=checkbox]:eq(0)');
                    firstCheckbox.prop('checked', true);
                    expect(firstCheckbox.prop('checked')).toBe(true);
                    button.click();
                    expect(interactor.onItemChange).toHaveBeenCalled();
                });
            });
            it("fills filter options when needed", function(){
                spyOn(util, 'fillCollectionChildSelect').and.callThrough();
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var spy = SpecLib.setSpyOnIfPossible(interactor.repo, 'getCollection');
                    spy.and.callFake(function(collectionName, callback){
                        callback('', [{name: 'testOption'}]);
                        if (interactor.childCollectionSelect !== undefined){
                            expect(util.fillCollectionChildSelect).toHaveBeenCalled();
                            var select = $('#' + name + '-' + childCollectionName + '-filter' + '-select');
                            expect(select.val()).toBe('Todos');
                            expect(select.children().length).toBe(2);
                            expect(select.children()[1].text).toBe('testOption');
                        }
                    });
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                });
            });
            it("filters collectionChild items when on filter", function(){
                util.runCallbackForEachArrayItem(collectionNamesWithChildCollections, function(name){
                    var interactor = app[name + 'Interactor'];
                    var childCollectionName = interactor.childCollectionName;
                    var validItemName = ((name === 'grupos')?'ficha0' : 'caracteristica0');
                    var spy = SpecLib.setSpyOnIfPossible(interactor.repo, 'getCollection');
                    var panel = $('#' + name + '-' + childCollectionName + '-panel');
                    spy.and.callFake(function(collectionName, callback){
                        callback('', [{name: 'testOption'}]);
                    });
                    spyOn(interactor.paintInteractor, 'filterCollectionChild').and.callFake(function(itemName, callback){
                        callback([]);
                        var firstDiv = panel.find('div:eq(0):visible');
                        expect(firstDiv.length).toBe(0);
                        callback([{name: validItemName}]);
                        firstDiv = panel.find('div:eq(0):visible');
                        expect(firstDiv.length).toBe(1);
                    });
                    interactor.paintInteractor.paintCollectionChildWithValue(testCollections[name], testItems[name]);
                    var select = $('#' + name + '-' + childCollectionName + '-filter' + '-select');
                    select.val('testOption');
                    select.change();
                });
            });
        });
    });

})(SpecLib);