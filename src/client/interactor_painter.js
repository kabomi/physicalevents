
var berlin = berlin || {};
berlin.Interactors = berlin.Interactors || {};

(function(){
    "use strict";

    function paintInteractor(interactor) {
        var self = {};
        var util = berlin.Util;
        var collectionName = interactor.getCollectionName();
        var repo = interactor.repo;

        function paintCollectionItem(item){
            if (item === null ||
                item === undefined) return;


            for (var name in interactor.editTags){
                if (interactor.editTags.hasOwnProperty(name)){
                    var tag = interactor.editTags[name];
                    var itemFieldValue = item[name];
                    util.setFieldFromTagWithValue(tag, itemFieldValue, function(value){
                        interactor.onItemChange(item, value);
                    });
                    self.fillControlOptions(tag, itemFieldValue);
                }
            }
            if (interactor.childCollectionUlSelect === undefined) return;
            repo.getCollection(interactor.childCollectionUlSelect, function(uri, collection){
                if (collection === null) collection = [];
                util.fillCollectionChildSelect(collectionName + '-' + interactor.childCollectionName + '-ul',
                        collection,
                        self.filterCollectionChild);
            });
        }
        function fillControlOptions(tag, value){
            if (tag.type === 'select'){
                repo.getCollection(tag.collectionName, function(uri, collection){
                    if (collection === null) collection = [];

                    var exclude = value;
                    util.setOptionsFromTagWithCollection(tag, collection, exclude);
                });
            }
        }
        function filterCollectionChild(itemName, callback){
            if (interactor.collectionName === 'grupos'){
                repo.getCollection(interactor.childCollectionName, function(uri, collection){
                    if (collection === null ||
                        util.isNotArray(collection[interactor.childCollectionName])){
                        return callback([]);
                    }
                    var filteredCollection = [];
                    collection.forEach(function(childCollectionItem){
                        if (childCollectionItem.grupo === itemName){
                            filteredCollection.push(childCollectionItem);
                        }
                    });

                    return callback(filteredCollection);
                });
            }else{
                repo.getCollectionItem(interactor.childCollectionSelect, itemName, function(uri, collectionItem){
                    if (collectionItem === null ||
                        util.isNotArray(collectionItem[interactor.childCollectionName])){
                        return callback([]);
                    }
                    var filteredCollection = [];
                    collectionItem[interactor.childCollectionName].forEach(function(childCollectionItem){
                        filteredCollection.push(childCollectionItem);
                    });

                    return callback(filteredCollection);
                });
            }
        }
        function paintCollection(){
            repo.getCollection(collectionName, function(uri, collection){
                if (collection === null)
                    collection = [];
                self.buildTableFromCollection(collection);
            });
        }
        function buildTableFromCollection(array){
            var actions = interactor.getTableActions();
            util.buildCollectionTable(collectionName, array, actions);
        }

        function paintCollectionChildWithValue(collection, item){
            var options = {
                item: item,
                collection: collection,
                collectionName: collectionName,
                childCollectionName: interactor.childCollectionName
            };
            util.setChildCollectionControls(options, function(value){
                interactor.onItemChange(item, value);
            });
            if (interactor.childCollectionSelect === undefined) return;
            repo.getCollection(interactor.childCollectionSelect, function(uri, collection){
                if (collection === null) collection = [];
                util.fillCollectionChildSelect(collectionName + '-' + interactor.childCollectionName,
                        collection,
                        self.filterCollectionChild);
            });
        }

        self.paintCollectionItem = paintCollectionItem;
        self.fillControlOptions = fillControlOptions;
        self.filterCollectionChild = filterCollectionChild;
        self.paintCollection = paintCollection;
        self.buildTableFromCollection = buildTableFromCollection;
        self.paintCollectionChildWithValue = paintCollectionChildWithValue;

        return self;
    }

    function fichasPaintInteractor(interactor) {
        var self = new paintInteractor(interactor);
        var util = berlin.Util;

        self.imageForm = {
            id: interactor.getCollectionName(),
            action: '/image'
        };

        var _paintCollectionItem = self.paintCollectionItem;
        function paintCollectionItem(item){
            var itemId = (item?item.id: undefined);

            util.setImageForm(itemId, self.imageForm);

            _paintCollectionItem(item);
        }


        self.paintCollectionItem = paintCollectionItem;
        
        return self;
    }

    function eventosPaintInteractor(interactor) {
        var self = new paintInteractor(interactor);
        var util = berlin.Util;

        var _paintCollectionItem = self.paintCollectionItem;
        function paintCollectionItem(item){
            util.setClassificationButton(interactor.getCollectionName(), item);

            _paintCollectionItem(item);
        }


        self.paintCollectionItem = paintCollectionItem;
        
        return self;
    }


    berlin.Interactors.paintInteractor = paintInteractor;
    berlin.Interactors.fichasPaintInteractor = fichasPaintInteractor;
    berlin.Interactors.eventosPaintInteractor = eventosPaintInteractor;
})(berlin);