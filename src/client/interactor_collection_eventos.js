/* jshint -W083 */

var berlin = berlin || {};
berlin.Interactors = berlin.Interactors || {};

(function(){
    "use strict";

    function eventosCollectionInteractor(name, app){
        var collectionName = name;
        var Factory = berlin.Factory;
        var util = berlin.Util;
        var self = new berlin.Interactors.collectionInteractor(name, app);


        self.handlers.showResult = function(item){
                var decodeItem = util.decodeUri(item);
                self.onEdit(decodeItem, onShowResult);};

        var _addRoutes = self.addRoutes;
        function addRoutes(){
            _addRoutes();
            self.routes.showResult = crossroads.addRoute('/' + collectionName + '/{id}/' + 'clasificacion', self.handlers.showResult);
        }
        var _getTableActions = self.getTableActions;
        function getTableActions(){
            var actions = _getTableActions();
            actions.LANZAR = onShowResultClick;

            return actions;
        }


        function onShowResultClick(item){
            if (item === null ||
                typeof(item) !== 'object') return;

            self.handlers.showResult(item);
            self.history.pushState('/' + collectionName + '/' + item.name + '/clasificacion', 'CLASIFICACION');
        }
        function onShowResult(item){
            if (util.isDefined(item) &&
                self.childCollectionName){

                var hasResults = isAlreadyCalculated(item);
                var hasValidChildCollection = self.hasValidChildCollectionValues(item);

                if (!hasResults &&
                    !hasValidChildCollection){

                    util.alert("Para poder ejectuar la accion " + self.childCollectionName + " deben sumar 100 en total");
                    return;
                }
                if (item.lesion === undefined || item.lesion < 0){
                    util.alert("Para poder ejecutar la accion el valor de lesion debe estar comprendido entre 0 y 100");
                    return;
                }
                if (item.grupo === undefined){
                    util.alert("Para poder ejecutar la accion debe seleccionar un grupo");
                    return;
                }

                self.calculateResults(item, function(calculatedItem){
                    resolveResultsView();
                    self.paintResults(calculatedItem);
                });
            }
        }
        function isAlreadyCalculated(item){
            if (util.isDefined(item) &&
                util.isArray(item.results)){

                return true;
            }
            return false;
        }
        function hasValidChildCollectionValues(item){
            if (util.isArray(item.results)) return true;
            var childCollection = item[self.childCollectionName];
            if (util.isNotArray(childCollection)) return false;

            var sum = 0;
            childCollection.forEach(function(item){
                if (item.value > 0) sum = sum + parseInt(item.value);
            });
            return (sum === 100);
        }
        function calculateResults(item, callback){
            if (isAlreadyCalculated(item)){
                callback(item);
                return;
            }
            self.repo.getResults(collectionName, item.id, function(uri, jsonResult){
                if (jsonResult){
                    callback(jsonResult);
                }
            });
        }
        function resolveResultsView(){
            self.editPanel.hide();
            self.resultsPanel.show();
        }
        function paintResults(item){
            console.dir(item);
            if (util.isArray(item.results)){
                var panel = Factory.createPanel('results-body');
                panel.empty();

                item.results.forEach(function(result, index){
                    var isLesionado = (result.lesion !== undefined);
                    var div = $('<div>');

                    var spanPos = $('<span>');
                    spanPos.addClass('badge pull-left');
                    spanPos.css('background-color', 'transparent');

                    var imgPath = "diploma.png";
                    if (index === 0) imgPath = "Trophy-gold.png";
                    if (index === 1) imgPath = "Trophy-silver.png";
                    if (index === 2) imgPath = "Trophy-bronze.png";
                    if (isLesionado) imgPath = "medic.png";
                    var img = $('<img>');
                    img.attr('src', "/assets/images/" + imgPath);
                    img.appendTo(spanPos);

                    spanPos.append("&nbps;");
                    spanPos.appendTo(div);

                    var a = $('<a>');
                    a.addClass('thumbnail');
                    var imgFicha = $('<img>');
                    imgFicha.attr('src', '/images/' + result.ficha.id);
                    imgFicha.appendTo(a);
                    a.appendTo(div);

                    var h2 = $('<h2>');
                    var spanNick = $('<span>');
                    spanNick.addClass('label label-info');
                    spanNick.append(result.ficha.nick);
                    spanNick.appendTo(h2);
                    var spanRes = $('<span>');
                    spanRes.addClass("badge pull-right");
                    spanRes.append(result.resultado);
                    spanRes.appendTo(h2);
                    if (isLesionado){
                        var h4 = $('<h4>');
                        h4.addClass('pull-right');
                        var spanLesionText = $('<span>');
                        spanLesionText.addClass("label label-danger");
                        spanLesionText.append(result.lesion.description);
                        spanLesionText.appendTo(h4);
                        h4.append('&nbsp;&nbsp;');
                        h4.appendTo(h2);
                    }
                    h2.appendTo(div);

                    div.appendTo(panel.nativeWidget);
                });
            }
        }


        self.addRoutes = addRoutes;
        self.getTableActions = getTableActions;
        self.hasValidChildCollectionValues = hasValidChildCollectionValues;
        self.calculateResults = calculateResults;
        self.paintResults = paintResults;

        return self;
    }


    berlin.Interactors.eventosCollectionInteractor = eventosCollectionInteractor;
})(berlin);