
var berlin = berlin || {};

(function(){
    "use strict";

    window.onpopstate = function(e){
        console.log("popState:" + document.location.pathname);
        crossroads.parse(document.location.pathname);
        e.preventDefault();
    };
    function App(){
        var self = {};
        var Factory = berlin.Factory;
        var util = berlin.Util;

        self.repo = Factory.createRepo('/data');
        self.history = Factory.createHistory();
        self.collectionNames = Factory.collectionNames;
        self.initButtons = Factory.createInitButtons();
        self.collectionPanels = Factory.createCollectionPanels();
        self.backPanel = Factory.createBackPanel();
        self.backButton = Factory.createBackButton();
        self.actionPanel = Factory.createActionPanel();
        self.resultsPanel = Factory.createResultsPanel();
        self.homeButton = Factory.createHomeButton();
        
        self.handlers = {
            home:    function(){ onHome();}
        };
        self.routes = {};
        
        function createAndInitializeInteractors(){
            util.runCallbackForEachArrayItem(self.collectionNames, function(name){
                var createMethod = 'create' + util.capitalizeFirstLetter(name) + 'Interactor';
                self[name + 'Interactor'] = Factory[createMethod](self);
                self[name + 'Interactor'].initialize();
            });
        }

        function initialize(){
            onHome();
            addRoutes();
            
            createAndInitializeInteractors();

            crossroads.ignoreState = true;
            self.backButton.onClick = onBackButtonClick;
            self.homeButton.onClick = onHomeButtonClick;
            if (util.isNotFiringOnPopStateEventOnLoad()){
                crossroads.parse(document.location.pathname);
            }
        }
        function onHome(){
            self.initButtons.show();
            self.backPanel.hide();
            self.collectionPanels.hide();
            self.actionPanel.hide();
            self.resultsPanel.hide();
        }
        function onBackButtonClick(){
            self.history.popState();
        }
        function onHomeButtonClick(){
            self.history.pushState('/');
            self.handlers.home();
        }
        function addRoutes(){
            self.routes.home =    crossroads.addRoute('/',        self.handlers.home);
        }


        self.initialize = initialize;

        return self;
    }

    berlin.App = App;
})(berlin);