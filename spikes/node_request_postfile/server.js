"use strict";

// This spike shows how to post a file using Node's EXPRESS module and request module.

(function(){
    console.log("Running Spike");
    var PORT = 3000;
    var SERVER = 'localhost';
    var express = require('express');
    var app = express();
    var fs = require("fs");
    var request = require("request");
    var SERVER_URI = "http://" + SERVER + ":" + PORT + "/";

    app.post("/", function(req, res){
        console.log("Received request");
        var image = req.files.image;
        fs.readFile(image.path, function (err, data) {
            if (err) throw err;
                res.end(data);
        });
    }).on('error', function(e) {
        console.log("Got error: " + e.message);
    });
    var r = request.post(SERVER_URI + '', function(response, responseData){
        try{
            done();
        }catch(ex){
            done(ex.message);
        }
    });
    var form = r.form();
    form.append('image', fs.createReadStream("file.html"));
    app.listen(PORT);
})();