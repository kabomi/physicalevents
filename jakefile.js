/*global desc, task, jake, fail, complete, directory */
(function(){
    "use strict";
    var pkg = require("./package");
    var NODE_VERSION = "v" + pkg.engines.node + "\n";
    var GENERATED_DIR = pkg.tempDirs.root;
    var TEMP_TESTFILE_DIR = GENERATED_DIR + pkg.tempDirs.test;

    var common = require('./build/build_options.js');
    var fs = require('fs');
    var lint = require("./build/lint/lint_runner.js");

    task("default", ["node", "lint", "test", "clean"]);

    // desc("Create temp testfiles directory");
    task("tempTestFileDir", function(){
        console.log("TESTFILE_DIR: " +  TEMP_TESTFILE_DIR);
        if (!fs.existsSync(GENERATED_DIR)){
            fs.mkdirSync(GENERATED_DIR);
        }
        if (!fs.existsSync(TEMP_TESTFILE_DIR)){
            fs.mkdirSync(TEMP_TESTFILE_DIR);
        }
    });
    desc("Delete all generated files");
    task("clean", [], function() {
        console.log("REMOVE TESTFILE_DIR: " +  TEMP_TESTFILE_DIR);
        jake.rmRf(GENERATED_DIR);
    });

    desc("Lint everything");
    task("lint", ["lintNode", "lintBrowser"]);

    task("lintNode", ["node"], function(){
        console.info("START NODE LINT");
        var files = new jake.FileList();
        files.include("**/*.js");
        files.exclude("node_modules", "build/karma.conf.js");

        var options = common.nodeLintOptions();
        var passed = lint.validateFileList(files.toArray(), options, {});
        if (!passed) fail("Lint failed");
    });
    task("lintBrowser", ["node"], function(){
        console.info("START BROWSER LINT");
        var files = new jake.FileList();
        files.include("src/client/**/*.js");
        files.exclude("node_modules");

        var options = common.browserLintOptions();
        var passed = lint.validateFileList(files.toArray(), options, {});
        if (!passed) fail("Lint failed");
    });

    desc("Test everything");
    task("test", ["testServer", "testClient"]);

    desc("Test server");
    task("testServer", ["node", "tempTestFileDir"], function(){
        console.info("START SERVER TESTS");
        testNodeJasmineWith.call(this, common.nodeJasmineOptions());
    }, {async: true});

    desc("Smoke tests");
    task("smoke", ["node", "tempTestFileDir"], function(){
        console.info("START SMOKE TESTS");
        testNodeJasmineWith.call(this, common.smokeJasmineOptions());
    }, {async: true});

    function testNodeJasmineWith(options){
        var jasmine = require("jasmine-node");
        options.onComplete = function(runner, log){
            if (runner.results().failedCount !== 0) {
                fail("Jasmine node tests failed.");
            }
            complete();
        };
        try {
            // since jasmine-node@1.0.28 an options object need to be passed
            jasmine.executeSpecsInFolder(options);
        } catch (e) {
            fail('Failed to execute "jasmine.executeSpecsInFolder": ' + e.stack);
            complete();
        }
    }

    desc("Test client");
    task("testClient", ["node", "tempTestFileDir"], function() {
        console.info("START CLIENT TESTS");
        sh("node node_modules/.bin/karma run build/karma.conf.js", function(msg, errCode) {
            if(errCode){
                fail("ERROR: " + msg);
            }else{
                console.log(msg);
            }
            console.log('AFTER TESTS');
            complete();
        });

    }, {async: true});

    desc("Integrate");
    task("integrate", ["default"], function() {
        console.info("START INTEGRATION");
        var commands = require('./build/integration_commands');
        commands.print();
    });

    // desc("Ensure correct version of Node is present");
    task("node", [], function() {
        sh("node --version", function(msg) {
            console.log(msg);
            if (msg !== NODE_VERSION){
                fail("Incorrect node version. Expected " + NODE_VERSION);
            }
            complete();
        });
    }, {async: true});

    function sh(command, callback) {
        console.log("> " + command);

        var stdout = "";
        var process = jake.createExec(command, {printStdout:true, printStderr: true});
        process.on("stdout", function(chunk) {
            stdout += chunk;
        });
        process.on("cmdEnd", function() {
            callback(stdout);
        });
        process.on("error", function(errMsg, errCode) {
            callback(errMsg, errCode);
        });
        process.run();
    }
}());
